export SOURCEPATH=	${HOME}/src/ohm/aco
export CLASSPATH=	${SOURCEPATH}/classfiles

export JAVAC=		/opt/java/bin/javac
export JAVACFLAGS=	-d ${CLASSPATH} -sourcepath ${SOURCEPATH}

all: aco

aco:
	${MAKE} -e -C aco

protoas:
	${MAKE} -e -C protoas

si:
	${MAKE} -e -C si/ant/graph
	${MAKE} -e -C si/ant/tsp

clean:
	find ${CLASSPATH} -name '*.class' -delete

.PHONY: clean aco protoas si
