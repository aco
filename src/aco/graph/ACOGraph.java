package aco.graph;

import java.util.HashMap;

import aco.misc.*;
import aco.mediator.*;
import aco.graph.data.*;

public class ACOGraph {

  protected ACOMediator acom;

  protected CoordinateData coordinateData;
  protected DistanceData distanceData;
  protected PheromoneData pheromoneData;
  protected ChoiceInformation choiceInformation;
  protected HeuristicInformation heuristicInformation;
  protected NearestNeighbourList nearestNeighbourList;

  public ACOGraph(ACOMediator acom) {
    this.acom = acom;

    this.coordinateData = null;
    this.distanceData = null;
    this.pheromoneData = null;
    this.choiceInformation = null;
    this.heuristicInformation = null;
    this.nearestNeighbourList = null;
  }

  public ACOGraph(ACOMediator acom, CoordinateData coordinateData) {
    this.acom = acom;

    acom.setACOGraph(this);

    initializeFromCoordinates(coordinateData);
  }

  public void initializeFromCoordinates(CoordinateData coordinateData) {
    this.coordinateData = coordinateData;

    acom.setNumOfCities(coordinateData.getNumOfCities());
    acom.computeNearestNeighbourListDepth();

    this.distanceData = new DistanceData(acom.computeDistances());
    this.nearestNeighbourList = new NearestNeighbourList(acom);
    this.pheromoneData = new PheromoneData(acom);
    this.choiceInformation = new ChoiceInformation(acom);
    this.heuristicInformation = new HeuristicInformation(acom);

    acom.computeHeuristicInformation();
    nearestNeighbourList.computeNearestNeighbours();
  }

  public int getNearestNeighbour(int x, int y) {
    return nearestNeighbourList.getNearestNeighbour(x, y);
  }

  public int getDistance(int x, int y) {
    return distanceData.getDistance(x, y);
  }

  public void setDistance(int x, int y, int v) {
    distanceData.setDistance(x, y, v);
  }

  public CoordinatePair<Integer, Integer> getCoordinates(int City) {
    return coordinateData.getCoordinates(City);
  }

  public HashMap<Integer, CoordinatePair<Integer, Integer>> getCoordinateData() {
    return coordinateData.getCoordinateData();
  }

  public double getPheromone(int x, int y) {
    return pheromoneData.getPheromone(x, y);
  }

  public void setPheromone(int x, int y, double v) {
    pheromoneData.setPheromone(x, y, v);
  }

  public double getChoiceInformation(int x, int y) {
    return choiceInformation.getChoiceInformation(x, y);
  }

  public void setChoiceInformation(int x, int y, double v) {
    choiceInformation.setChoiceInformation(x, y, v);
  }

  public double getHeuristicInformation(int x, int y) {
    return heuristicInformation.getHeuristicInformation(x, y);
  }
  
  public void setHeuristicInformation(int x, int y, double v) {
    heuristicInformation.setHeuristicInformation(x, y, v);
  }

}

/*
  @Override
    public String toString() {
      StringBuilder result = new StringBuilder();

      if (getIds() != null) {
        for (String id : getIds())
          result.append("\t" + id);
      } else {
        for (int i = 0; i < gp.getNumOfCities(); i++)
          result.append("\t" + i);
      }

      int l = 0;
      for (int i[] : distance.getDistance()) {
        if (ids != null) {
          if (ids[l] != null) {
            result.append("\n" + ids[l++] + "\t");
          } else {
            result.append("\n" + (l++) + "\t");
          }
        } else {
            result.append("\n" + (l++) + "\t");
        }

        for (int j : i) {
          if (j == gp.getInfinity()) {
            result.append("INF\t");
          } else {
            result.append(j + "\t");
          }
        }
      }

      return result.toString();
    }

  public static void main (String[] args) {
    try {
      Graph graph;
      if (args.length == 1)
        graph = new Graph(args[0]);
      else
        graph = Graph.sampleGraph();
      System.out.println(graph);
      graph.printNearestNeighbours();
    } catch (Exception e) {
      System.out.println(e.toString() + "(" + e.getClass() + "): " + e);
      System.exit(1);
    }
  }

}
*/
