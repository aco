package aco.graph.data;

import aco.mediator.*;

public class PheromoneData {

  protected double[][] pheromoneData;

  protected ACOMediator acom;

  public PheromoneData(ACOMediator acom) {
    this.acom = acom;

    this.pheromoneData = new double[acom.getNumOfCities()][acom.getNumOfCities()];

    for (int i = 0; i < acom.getNumOfCities(); i++) {
      for (int j = 0; j < acom.getNumOfCities(); j++) {
        setPheromone(i,j, 0 );
      }
    }

  }

  public double getPheromone(int x, int y) {
    return this.pheromoneData[x][y];
  }

  public void setPheromone(int x, int y, double pheromone) {
    this.pheromoneData[x][y] = pheromone;
    this.pheromoneData[y][x] = pheromone;
  }

}
