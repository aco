package aco.graph.data;

import aco.mediator.*;

public class NearestNeighbourList {

  protected int[][] nearestNeighbourList;

  protected ACOMediator acom;

  public NearestNeighbourList(ACOMediator acom) {
    this.acom = acom;

    this.nearestNeighbourList =
      new int[acom.getNumOfCities()][acom.getNearestNeighbourListDepth()];

    for (int i = 0; i < acom.getNumOfCities(); i++) {
      for (int j = 0; j < acom.getNearestNeighbourListDepth(); j++) {
        setNearestNeighbour(i, j, -1);
      }
    }
  }

  public void computeNearestNeighbours() {
    for (int City = 0; City < acom.getNumOfCities(); City++) {
      for (int Neighbour = 0; Neighbour < acom.getNumOfCities(); Neighbour++) {

        if (City == Neighbour) {
          continue;
        } else {

          for (int d = 0; d < acom.getNearestNeighbourListDepth(); d++) {

            /* if there is no entry yet, then assign */
            if (getNearestNeighbour(City,d) == -1) {
              setNearestNeighbour(City,d, Neighbour);
              break;
            }
            /* if distance from city to neighbour is smaller
             * than distance from city to neighbour from list */
            if (acom.getDistance(City,Neighbour) <
                acom.getDistance(City, getNearestNeighbour(City,d))) {

              /* copy element n-1 to n; right shift so to say; until elem d is reached */
              for (int c = acom.getNearestNeighbourListDepth() - 1; c > d; c--) {
                setNearestNeighbour(City,c, getNearestNeighbour(City,c-1));
              }
              setNearestNeighbour(City,d, Neighbour);
              break;

                }

          }
        }
      }
    }
  }

  public int getNearestNeighbour(int x, int y) {
    return this.nearestNeighbourList[x][y];
  }

  public void setNearestNeighbour(int x, int y, int nearestNeighbour) {
    this.nearestNeighbourList[x][y] = nearestNeighbour;
  }

}
