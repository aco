package aco.graph.data;

import aco.mediator.*;

public class HeuristicInformation {

  protected double[][] heuristicInformation;

  protected ACOMediator acom;

  public HeuristicInformation(ACOMediator acom) {
    this.acom = acom;

    this.heuristicInformation = new double[acom.getNumOfCities()][acom.getNumOfCities()];
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      for (int j = 0; j < acom.getNumOfCities(); j++) {
        setHeuristicInformation(i,j, 0);
      }
    }
  }

  public double getHeuristicInformation(int x, int y) {
    return this.heuristicInformation[x][y];
  }

  public void setHeuristicInformation(int x, int y, double heuristicInformation) {
    this.heuristicInformation[x][y] = heuristicInformation;
    this.heuristicInformation[y][x] = heuristicInformation;
  }

}
