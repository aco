package aco.graph.data;

import aco.mediator.*;

public class ChoiceInformation {

  protected double[][] choiceInformation;

  protected ACOMediator acom;

  public ChoiceInformation(ACOMediator acom) {
    this.acom = acom;

    this.choiceInformation = new double[acom.getNumOfCities()][acom.getNumOfCities()];

    for (int i = 0; i < acom.getNumOfCities(); i++) {
      for (int j = 0; j < acom.getNumOfCities(); j++) {
        setChoiceInformation(i,j, 0);
      }
    }
  }

  public double getChoiceInformation(int x, int y) {
    return this.choiceInformation[x][y];
  }

  public void setChoiceInformation(int x, int y, double choiceInformation) {
    this.choiceInformation[x][y] = choiceInformation;
    this.choiceInformation[y][x] = choiceInformation;
  }

}
