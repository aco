package aco.graph.data;

import aco.misc.*;
import aco.tsplibreader.*;

import java.util.HashMap;

public class CoordinateData {

  protected final HashMap<Integer, CoordinatePair<Integer, Integer>> coordinateData;

  public CoordinateData(TSPLibReader tsplibreader) {
    this.coordinateData = tsplibreader.getCoordinates();
  }

  public CoordinatePair<Integer, Integer> getCoordinates(int City) {
    return coordinateData.get(City);
  }

  public HashMap<Integer, CoordinatePair<Integer, Integer>> getCoordinateData() {
    return coordinateData;
  }

  public int getNumOfCities() {
    return coordinateData.size();
  }

}
