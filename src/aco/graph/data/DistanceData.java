package aco.graph.data;

import aco.mediator.*;

public class DistanceData {

  protected int[][] distanceData;

  protected ACOMediator acom;

  public DistanceData(ACOMediator acom) {
    this.acom = acom;

    this.distanceData = new int[acom.getNumOfCities()][acom.getNumOfCities()];

    for (int i = 0; i < acom.getNumOfCities(); i++) {
      for (int j = 0; j < acom.getNumOfCities(); j++) {
        setDistance(i,j, acom.getInfinity());
      }
    }
  }

  public DistanceData(int[][] distanceData) {
    this.distanceData = distanceData;
  }

  public int getDistance(int x, int y) {
    return this.distanceData[x][y];
  }

  public void setDistance(int x, int y, int distance) {
    this.distanceData[x][y] = distance;
    this.distanceData[y][x] = distance;
  }

}
