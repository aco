package aco.antview;

import java.util.TreeSet;
import java.util.HashMap;
import java.util.Collections;
import java.awt.*;
import java.applet.*;

public class AntViewApplet
  extends Applet {

  final static long serialVersionUID = 1L;

  protected int xMax;
  protected int yMax;
  protected int wXSize;
  protected int wYSize;
  protected double xScale;
  protected double yScale;
  protected double xMax2yMaxRatio;
  protected int[] xPoints;
  protected int[] yPoints;
  protected final int cityPointRadius = 8;
  protected final int borderSize = 30;

  protected AntViewObservable avo;

  public AntViewApplet(AntViewObservable avo) {
    this(avo,
        (int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.9),
        (int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.9));
  }

  public AntViewApplet(
      AntViewObservable avo,
      int wsize) {
    this(avo, wsize, wsize);
  }

  public AntViewApplet(
      AntViewObservable avo,
      int wxsize,
      int wysize) {

    this.avo = avo;

    this.xPoints = new int[avo.getNumOfCities() + 1];
    this.yPoints = new int[avo.getNumOfCities() + 1];

    this.xMax = 0;
    this.yMax = 0;

    for (int c = 0; c < avo.getNumOfCities(); c++) {
      if (avo.getCoordinates(c).getFirst() > xMax)
        this.xMax = avo.getCoordinates(c).getFirst();
      if (avo.getCoordinates(c).getSecond() > yMax)
        this.yMax = avo.getCoordinates(c).getSecond();
    }

    this.xMax2yMaxRatio = (double)this.xMax/(double)this.yMax;

    this.wXSize = (int)((double)this.xMax/(double)this.yMax * (double)wxsize) + borderSize;
    this.wYSize = wysize + borderSize;
  }

  protected void computeScale(int width, int height) {
    if (width > (height * xMax2yMaxRatio)) {
      xScale = ((height - 2 * borderSize) * xMax2yMaxRatio) / (double)xMax;
      yScale = (height - 2 * borderSize) / (double)yMax;
    } else {
      xScale = (width - 2 * borderSize) / (double)xMax;
      yScale = ((width - 2 * borderSize) * 1/xMax2yMaxRatio) / (double)yMax;
    }
  }

  protected void scalePoints(int width, int height, int[] xpoints, int[] ypoints) {
    for (int c = 0; c < xpoints.length; c++) {
      xpoints[c] =
        (int)((double)xpoints[c] * xScale) + borderSize;
    }
    for (int c = 0; c < ypoints.length; c++) {
      ypoints[c] =
        height - (int)((double)ypoints[c] * yScale) - borderSize;
    }
  }

  public void paint(Graphics g) {
    Graphics2D g2 = (Graphics2D)g;
    HashMap<RenderingHints.Key, Object> rhmap = new HashMap<RenderingHints.Key, Object>(8);
    rhmap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_DEFAULT);
    rhmap.put(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    rhmap.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    rhmap.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
    rhmap.put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    rhmap.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    rhmap.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    rhmap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    RenderingHints rh = new RenderingHints(rhmap);
    g2.setRenderingHints(rh);

    this.update(g);
  }

  public void update(Graphics g) {

    Graphics2D g2 = (Graphics2D)g;
    Dimension d = this.getSize();

    computeScale(d.width, d.height);

    TreeSet<Integer> TourLengths = new TreeSet<Integer>(Collections.reverseOrder());
    TourLengths.addAll(avo.getGlobalBestTourMap().keySet());

    if (TourLengths.size() > 0) {

      g2.clearRect(0, 0, d.width, d.height);

      int x = TourLengths.size();
      for (int TourLength : TourLengths) {

        int[] GlobalBestTour = avo.getGlobalBestTourMap().get(TourLength);
        for (int c = 0; c < avo.getNumOfCities() + 1; c++) {
          xPoints[c] = avo.getCoordinates(
              GlobalBestTour[c] ).getFirst();
          yPoints[c] = avo.getCoordinates(
              GlobalBestTour[c] ).getSecond();
        }

        scalePoints(d.width, d.height, xPoints, yPoints);
        drawTourLines(g2, new Color(200/x, 20/x, 20/(x--)), 1.75f);
      }

      g2.setColor(new Color(0, 0, 0));
      g2.drawString("Minimum Length: " +
          TourLengths.last() +
          " at Iteration " +
          avo.getGlobalBestTourIteration(),
          10, d.height - 10);

    }

    drawCities(g2);
  }

  public void drawCities(Graphics2D g2) {
    Dimension d = this.getSize();

    int cpr = (int)cityPointRadius;
    if (d.getWidth() > d.getHeight())
      cpr = (int)Math.round(d.getHeight()/wYSize * cityPointRadius);
    else
      cpr = (int)Math.round(d.getWidth()/wXSize * cityPointRadius);

    if (cpr < 2)
      cpr = 2;

    int halfcpr = cpr >> 1;
    for (int c = 0; c < avo.getNumOfCities(); c++) {
      g2.setColor(new Color(0, 0, 128));
      g2.fillOval(xPoints[c] - halfcpr,
          yPoints[c] - halfcpr,
          cpr,
          cpr);
      g2.setColor(new Color(0, 128, 0));
      g2.drawString(Integer.toString(c),
          xPoints[c] + halfcpr,
          yPoints[c] - halfcpr);
    }

  }

  protected void drawCityWeb(Graphics2D g2) {
    g2.setColor(new Color(210, 210, 210));
    g2.setStroke(new BasicStroke(0.25f));

    for (int c1 = 0; c1 < avo.getNumOfCities(); c1++) {
      for (int c2 = 0; c2 < avo.getNumOfCities(); c2++) {
        g2.drawLine(xPoints[c1], yPoints[c1], xPoints[c2], yPoints[c2]);
      }
    }
  }

  protected void drawTourLines(Graphics2D g2, Color color, float strokestrength) {
    g2.setColor(color);
    //g2.setColor(new Color(200, 20, 20));
    g2.setStroke(new BasicStroke(strokestrength));
    g2.drawPolyline(xPoints, yPoints, avo.getNumOfCities() + 1);
  }

  public void update() {
    Graphics2D g2 = (Graphics2D)this.getGraphics();
    if (g2 != null)
      this.update(g2);
    else
      return;
  }

}
