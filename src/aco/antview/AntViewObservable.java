package aco.antview;

import java.util.Map;

import aco.misc.CoordinatePair;

public interface AntViewObservable {

  /* the number of cities to draw */
  public int getNumOfCities();

  /* the iteration of the global best tour */
  public int getGlobalBestTourIteration();

  /* the global best tours (probably only 5 or 10 of them)
   * Key = GlobalBestTourLength, Value = GlobalBestTour[] */
  public Map<Integer, int[]> getGlobalBestTourMap();

  /* get a pair of integer coordinates for a given city */
  public CoordinatePair<Integer, Integer> getCoordinates(int City);

}
