package aco.antview;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.util.Observer;
import java.util.Observable;

public class AntView
  extends Observable
  implements Observer, Runnable, WindowListener {

  protected JFrame jframe;
  protected AntViewApplet ava;

  public AntView(AntViewObservable avo) {
    this.ava = new AntViewApplet(avo);
  }

  public AntView(AntViewApplet ava) {
    this.ava = ava;
  }

  public void setAntViewObservable(AntViewObservable avo) {
    int wxsize = (int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.9);
    int wysize = (int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.9);

    this.ava = new AntViewApplet(avo);
    ava.computeScale(wxsize, wysize);

    Container cp = jframe.getContentPane();

    for (Component c : cp.getComponents())
      cp.remove(c);

    cp.add(BorderLayout.CENTER, this.ava);

    jframe.pack();
    jframe.setSize(new Dimension(ava.wXSize, ava.wYSize));
    jframe.setVisible(true);
    jframe.toFront();
    jframe.addWindowListener(this);
  }

  public void run() {
    int wxsize = (int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.9);
    int wysize = (int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.9);

    ava.computeScale(wxsize, wysize);

    jframe = new JFrame("Custom...");

    jframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

    Container cp = jframe.getContentPane();
    cp.setBackground(Color.white);
    cp.add(BorderLayout.CENTER, this.ava);

    jframe.pack();
    jframe.setSize(new Dimension(ava.wXSize, ava.wYSize));
    jframe.setVisible(true);
    jframe.toFront();
    jframe.addWindowListener(this);
  }

  public void update(Observable o, Object arg) {
    ava.update();
  }

  public void windowActivated(WindowEvent e) { }

  public void windowClosed(WindowEvent e) {
    this.setChanged();
    this.notifyObservers();
  }

  public void windowClosing(WindowEvent e) {
    ava.destroy();
    jframe.dispose();
  }

  public void windowDeactivated(WindowEvent e) { }

  public void windowDeiconified(WindowEvent e) { }

  public void windowIconified(WindowEvent e) { }

  public void windowOpened(WindowEvent e) { }

}
