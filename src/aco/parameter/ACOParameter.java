package aco.parameter;

import aco.strategy.*;

public abstract class ACOParameter {

  protected final int Infinity = Integer.MAX_VALUE;
  protected double Beta;
  protected double Roh;
  protected double TauZero;

  protected int NumOfAnts;
  protected int NumOfCities;
  protected int MaxNumOfTours;
  protected int NearestNeighbourListDepth;
  protected int GlobalBestTourMapSize;

  protected ACOStrategy acos;
  protected GraphStrategy gs;
  protected TauZeroStrategy tzs;
  protected DistanceStrategy ds;
  protected PheromoneStrategy ps;
  protected ChoiceInformationStrategy cis;
  protected HeuristicInformationStrategy his;

  public ACOParameter() {
    this.Beta = 0.0;
    this.Roh = 0.0;
    this.TauZero = 0.0;

    this.NumOfCities = 0;
    this.NumOfAnts = 0;
    this.MaxNumOfTours = 0;
    this.NearestNeighbourListDepth = 0;

    this.acos = null;
    this.gs = null;
    this.tzs = null;
    this.ds = null;
    this.ps = null;
    this.cis = null;
    this.his = null;
  }

  public ACOParameter(int NumOfCities) {
    this.Beta = 0.0;
    this.Roh = 0.0;
    this.TauZero = 0.0;

    this.MaxNumOfTours = 0;
    this.NumOfCities = NumOfCities;
    this.NumOfAnts = NumOfCities;

    this.acos = null;
    this.gs = null;
    this.tzs = null;
    this.ds = null;
    this.ps = null;
    this.cis = null;
    this.his = null;
  }

  public ACOParameter(
      ACOStrategy acos,
      GraphStrategy gs,
      TauZeroStrategy tzs,
      DistanceStrategy ds,
      PheromoneStrategy ps,
      ChoiceInformationStrategy cis,
      HeuristicInformationStrategy his) {
    this(0.0, 0.0, 0.0,
        0, 0, 0, 0,
        acos, gs, tzs, ds, ps, cis, his);
  }

  public ACOParameter(
      int NumOfCities,
      ACOStrategy acos,
      GraphStrategy gs,
      TauZeroStrategy tzs,
      DistanceStrategy ds,
      PheromoneStrategy ps,
      ChoiceInformationStrategy cis,
      HeuristicInformationStrategy his) {
    this(0.0, 0.0, 0.0,
        0, NumOfCities, NumOfCities, NumOfCities,
        acos, gs, tzs, ds, ps, cis, his);
  }

  public ACOParameter(
      int NumOfAnts, int NumOfCities, int NearestNeighbourListDepth,
      ACOStrategy acos,
      GraphStrategy gs,
      TauZeroStrategy tzs,
      DistanceStrategy ds,
      PheromoneStrategy ps,
      ChoiceInformationStrategy cis,
      HeuristicInformationStrategy his) {
    this(0.0, 0.0, 0.0,
        0, NumOfAnts, NumOfCities, NearestNeighbourListDepth,
        acos, gs, tzs, ds, ps, cis, his);
  }

  public ACOParameter(
      double Beta, double Roh, double TauZero,
      int MaxNumOfTours, int NumOfAnts, int NumOfCities,
      int NearestNeighbourListDepth,
      ACOStrategy acos,
      GraphStrategy gs,
      TauZeroStrategy tzs,
      DistanceStrategy ds,
      PheromoneStrategy ps,
      ChoiceInformationStrategy cis,
      HeuristicInformationStrategy his) {

    this.Beta = Beta;
    this.Roh = Roh;
    this.TauZero = TauZero;

    this.MaxNumOfTours = 0;
    this.NumOfAnts = NumOfAnts;
    this.NumOfCities = NumOfCities;
    this.NearestNeighbourListDepth = NearestNeighbourListDepth;

    this.acos = acos;
    this.gs = gs;
    this.tzs = tzs;
    this.ds = ds;
    this.ps = ps;
    this.cis = cis;
    this.his = his;
  }

  public int getInfinity() {
    return this.Infinity;
  }

  public double getBeta() {
    return this.Beta;
  }

  public void setBeta(double Beta) {
    this.Beta = Beta;
  }

  public double getRoh() {
    return this.Roh;
  }

  public void setRoh(double Roh) {
    this.Roh = Roh;
  }

  public double getTauZero() {
    return this.TauZero;
  }

  public void setTauZero(double v) {
    this.TauZero = v;
  }

  public int getNumOfAnts() {
        return this.NumOfAnts;
  }

  public void setNumOfAnts(int NumOfAnts) {
        this.NumOfAnts = NumOfAnts;
  }

  public int getNumOfCities() {
        return this.NumOfCities;
  }

  public void setNumOfCities(int NumOfCities) {
        this.NumOfCities = NumOfCities;
  }

  public int getMaxNumOfTours() {
        return this.MaxNumOfTours;
  }

  public void setMaxNumOfTours(int MaxNumOfTours) {
        this.MaxNumOfTours = MaxNumOfTours;
  }

  public int getNearestNeighbourListDepth() {
        return this.NearestNeighbourListDepth;
  }

  public void setNearestNeighbourListDepth(int NearestNeighbourListDepth) {
        this.NearestNeighbourListDepth = NearestNeighbourListDepth;
  }

  public int getGlobalBestTourMapSize() {
    return this.GlobalBestTourMapSize;
  }

  public void setGlobalBestTourMapSize(int GlobalBestTourMapSize) {
    this.GlobalBestTourMapSize = GlobalBestTourMapSize;
  }

  public ACOStrategy getACOStrategy() {
    return acos;
  }

  public GraphStrategy getGraphStrategy() {
    return gs;
  }

  public TauZeroStrategy getTauZeroStrategy() {
    return tzs;
  }

  public DistanceStrategy getDistanceStrategy() {
    return ds;
  }

  public PheromoneStrategy getPheromoneStrategy() {
    return ps;
  }

  public ChoiceInformationStrategy getChoiceInformationStrategy() {
    return cis;
  }

  public HeuristicInformationStrategy getHeuristicInformationStrategy() {
    return his;
  }

  public void setACOStrategy(ACOStrategy acos) {
    this.acos = acos;
  }

  public void setGraphStrategy(GraphStrategy gs) {
    this.gs = gs;
  }

  public void setTauZeroStrategy(TauZeroStrategy tzs) {
    this.tzs = tzs;
  }

  public void setDistanceStrategy(DistanceStrategy ds) {
    this.ds = ds;
  }

  public void setPheromoneStrategy(PheromoneStrategy ps) {
    this.ps = ps;
  }

  public void setChoiceInformationStrategy(ChoiceInformationStrategy cis) {
    this.cis = cis;
  }

  public void setHeuristicInformationStrategy(HeuristicInformationStrategy his) {
    this.his = his;
  }

}
