package aco.parameter.acs;

import aco.parameter.*;

public class ACSParameter
  extends ACOParameter {

  protected double qZero;

  public ACSParameter() {
    super();
  }

  public double getQZero() {
    return this.qZero;
  }

  public void setQZero(double qZero){
    this.qZero = qZero;
  }

  @Override
    public String toString() {
      StringBuilder result = new StringBuilder();
      result.append("Roh: " + Roh + "\n");
      result.append("Beta: " + Beta + "\n");
      result.append("QZero: " + qZero + "\n");
      result.append("TauZero: " + TauZero + "\n");
      result.append("NumOfAnts: " + NumOfAnts + "\n");
      result.append("NumOfCities: " + NumOfCities + "\n");
      result.append("MaxNumOfTours: " + MaxNumOfTours + "\n");
      result.append("NearestNeighbourListDepth: " + NearestNeighbourListDepth + "\n");

      return result.toString();
    }

}
