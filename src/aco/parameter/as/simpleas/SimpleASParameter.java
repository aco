package aco.parameter.as.simpleas;

import aco.strategy.*;
import aco.parameter.as.*;

public class SimpleASParameter
  extends ASParameter {

  public SimpleASParameter() {
    super();
  }

  public SimpleASParameter(
      int NumOfCities,
      ACOStrategy acos,
      GraphStrategy gs,
      TauZeroStrategy tzs,
      DistanceStrategy ds,
      PheromoneStrategy ps,
      ChoiceInformationStrategy cis,
      HeuristicInformationStrategy his) {

    super(1.0, 5.0, 0.5, 0.0001,
        0, NumOfCities, NumOfCities, NumOfCities,
        acos, gs, tzs, ds, ps, cis, his);
  }

  public SimpleASParameter(
      int NumOfAnts, int NumOfCities, int NearestNeighbourListDepth,
      ACOStrategy acos,
      GraphStrategy gs,
      TauZeroStrategy tzs,
      DistanceStrategy ds,
      PheromoneStrategy ps,
      ChoiceInformationStrategy cis,
      HeuristicInformationStrategy his) {

    super(1.0, 5.0, 0.5, 0.0001,
        0, NumOfAnts, NumOfCities, NearestNeighbourListDepth,
        acos, gs, tzs, ds, ps, cis, his);
  }

  public SimpleASParameter(
      double Alpha, double Beta, double Roh, double TauZero,
      int MaxNumOfTours, int NumOfAnts, int NumOfCities,
      int NearestNeighbourListDepth,
      ACOStrategy acos,
      GraphStrategy gs,
      TauZeroStrategy tzs,
      DistanceStrategy ds,
      PheromoneStrategy ps,
      ChoiceInformationStrategy cis,
      HeuristicInformationStrategy his) {

    this.Alpha = Alpha;
    this.Beta = Beta;
    this.Roh = Roh;
    this.TauZero = TauZero;

    this.MaxNumOfTours = 0;
    this.NumOfAnts = NumOfAnts;
    this.NumOfCities = NumOfCities;
    this.NearestNeighbourListDepth = NearestNeighbourListDepth;

    this.acos = acos;
    this.gs = gs;
    this.tzs = tzs;
    this.ds = ds;
    this.ps = ps;
    this.cis = cis;
    this.his = his;
  }

  @Override
    public String toString() {
      StringBuilder result = new StringBuilder();
      result.append("Alpha: " + Alpha + "\n");
      result.append("Beta: " + Beta + "\n");
      result.append("Roh: " + Roh + "\n");
      result.append("TauZero: " + TauZero + "\n");
      result.append("NumOfAnts: " + NumOfAnts + "\n");
      result.append("NumOfCities: " + NumOfCities + "\n");
      result.append("MaxNumOfTours: " + MaxNumOfTours + "\n");
      result.append("NearestNeighbourListDepth: " + NearestNeighbourListDepth + "\n");

      return result.toString();
    }

}
