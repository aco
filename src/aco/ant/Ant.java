package aco.ant;

import aco.mediator.*;

public class Ant {

  protected ACOMediator acom;

  public int TourLength; // the ant's tour length
  public int[] Tour; // N+1; ant's memory storing (partial) tours
  public boolean[] Visited; // N; visited cities

  public Ant(ACOMediator acom) {
    this.acom = acom;

    Tour = new int[acom.getNumOfCities() + 1];
    Visited = new boolean[acom.getNumOfCities()];

    this.reset();
  }

  public int getTour(int i) {
    return this.Tour[i];
  }

  public void setTour(int i, int c) {
    this.Tour[i] = c;
  }

  public boolean getVisited(int i) {
    return Visited[i];
  }

  public void setVisited(int i, boolean b) {
    Visited[i] = b;
  }

  public int getTourLength() {
    return this.TourLength;
  }

  public void setTourLength(int i) {
    this.TourLength = i;
  }

  public void reset() {
    TourLength = 0;

    for (int i = 0; i < acom.getNumOfCities() + 1; i++)
      setTour(i, 0);

    for (int i = 0; i < acom.getNumOfCities(); i++)
      setVisited(i, false);
  }

  /*
  @Override
    public String toString() {
      StringBuilder result = new StringBuilder();

      result.append("Tour with length: " + TourLength + "\n");

      for (int t : Tour) {
        if (graph.ids[t] != null)
          result.append(graph.ids[t] + "\t");
        else
          result.append(t + "\t");
      }
      if (graph.ids[0] != null)
          result.append(graph.ids[getTour(0)]);
        else
          result.append(Tour[0]);

      return result.toString();
    }
    */

}
