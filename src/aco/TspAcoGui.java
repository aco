package aco;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Observer;
import java.util.Observable;

import aco.antview.*;
import aco.mediator.*;
import aco.environment.*;

public class TspAcoGui
  extends JFrame
  implements PopupMenuListener, ItemListener, ActionListener, Observer {

  static final long serialVersionUID = 100000000L;

  protected AntView antView = null;
  protected ACOMediator acom = null;
  protected Thread antThread = null;
  protected final String tspdir = "aco/tspproblems/";

  String filename;// = "rat99.tsp";
  Container c;
  private int choice;
  JPanel jp1, jp2, jp3;
  JComboBox algorithms, cases;
  double alpha, beta,roh,q ;
  int  cl, runs,ew, w, ants;
  JTextField[] values;  // 0 = alpha, 1 = beta, 2 = roh, 3 = q, 4 = cl, 5 = w, 6 = e, 7 = runs, 8 = ants
  JLabel[] labels;
  JButton startButton, b2;
  JPanel[] pairs;

  String[] labelTags = {"\u03B1", "\u03B2", "\u03C1", "q", "Neighbours", "w",
    "e","Iterations", "ants"};
  String[] concreteAntAlgs = {"Ant System (AS)", "Elitist Ant System (EAS)",
    "Rank-based AS (ASR)", "Ant Colony System (ACS)"};
  String[] tips = {"parameter for decision rule", "parameter for decision rule",
    "parameter for update of pheromone concentration",
    "parameter for random selection of decision rule",
    "number of neighbours for each point",
    "number of Ants which deposit pheromones",
    "defines the weight given to the best-so-tour", "number of iterations",
    "number of ants used to run the algorithm"};

  /**GUI Constuctor for Ant Colony Optimization Algorithms*/
  public TspAcoGui() {
    choice = 0;
    c = getContentPane();
    jp1 = new JPanel(new FlowLayout(FlowLayout.CENTER, 15,15));//new GridLayout(0,1, 20, 20));
    jp2 = new JPanel(new GridLayout(0,2));
    jp3 = new JPanel(new FlowLayout(FlowLayout.CENTER, 15,15));
    algorithms = new JComboBox(concreteAntAlgs);
    algorithms.setSelectedIndex(0);
    jp1.add(algorithms);

    cases = new JComboBox();
    this.updateFileChooser();
    jp1.add(cases);

    values = new JTextField[9];
    labels = new JLabel[9];
    for(int i =0; i<labelTags.length; i++){
      labels[i] = new JLabel(labelTags[i], JLabel.CENTER);
      values[i] = new JTextField();
      values[i].setToolTipText(tips[i]);
      values[i].setSize(50, 70);
      labels[i].setFont(new Font("Sans Serif", Font.BOLD, 13));
      values[i].setFont(new Font("Sans Serif", Font.BOLD, 13));
    }
    setInitialValues();
    for(int j=0; j<3; j++){
      jp2.add(labels[j]);
      jp2.add(values[j]);
    }
    jp2.add(labels[7]);
    jp2.add(values[7]);
    jp2.add(labels[8]);
    jp2.add(values[8]);
    startButton = new JButton("Run selected Algorithm");
    startButton.setFont(new Font("Sans Serif", Font.BOLD, 13));
    startButton.addActionListener(this);
    jp3.add(startButton);
    c.add(jp1, BorderLayout.NORTH);
    c.add(jp2, BorderLayout.CENTER);
    c.add(jp3, BorderLayout.SOUTH);
    algorithms.addItemListener(this);
    cases.addPopupMenuListener(this);
    this.setTitle("TSP Ant Colony Optimization");
    this.setSize(400,385);
    this.setLocation(100,100);
    this.setVisible(true);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  protected void updateFileChooser() {
    File tspfile = new File(tspdir);
    cases.removeAllItems();
    ArrayList<String> tspfiles = new ArrayList<String>();

    for (String s : tspfile.list(new FilenameFilter() {
      public boolean accept(File dir, String name) {
        return name.matches(".*\\.tsp");
      }
    }
    ))
      tspfiles.add(s);

    Collections.sort(tspfiles);

    for (String s : tspfiles)
      cases.addItem(s);
  }

  /* only a stub method here */
  public void popupMenuCanceled(PopupMenuEvent e) {}

  /* only a stub method here */
  public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {}

  /* called before the popup menu will be shown */
  public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    this.updateFileChooser();
  }

  /**ActionListener for start button*/
  public void actionPerformed(ActionEvent e){
    boolean stop = false;
    switch(choice){
      case 0: //AS
        alpha = Double.parseDouble(values[0].getText());
        break;
      case 1: //elitist
        alpha = Double.parseDouble(values[0].getText());
        ew = Integer.parseInt(values[6].getText());
        if(ew < 0){
          labels[6].setForeground(Color.RED);
          stop = true;
        }
        break;
      case 2:   //ASR
        alpha = Double.parseDouble(values[0].getText());
        w = Integer.parseInt(values[5].getText());
        if(w < 0){
          labels[5].setForeground(Color.RED);
          stop = true;
        }
        break;
      case 3: //ACS
        alpha = 1;
        cl = Integer.parseInt(values[4].getText());
        q = Double.parseDouble(values[3].getText());
        if(q<0 || q > 1){
          labels[3].setForeground(Color.RED);
          stop = true;
        }
        if(cl<0){
          labels[3].setForeground(Color.RED);
          stop = true;
        }
    }

    ants = Integer.parseInt(values[8].getText());
    beta =  Double.parseDouble(values[1].getText());
    roh = Double.parseDouble(values[2].getText());
    runs = Integer.parseInt(values[7].getText());

    if(alpha<0 ){
      labels[0].setForeground(Color.RED);
      stop = true;
    }

    if(beta<0 ){
      labels[1].setForeground(Color.RED);
      stop = true;
    }

    if(roh<0 || roh > 1){
      labels[2].setForeground(Color.RED);
      stop = true;
    }

    if(runs < 0){
      labels[7].setForeground(Color.RED);
      stop = true;
    }

    if(ants < 0){
      labels[8].setForeground(Color.RED);
      stop = true;
    }

    if(stop)
      return;

    File tspfile = new File(tspdir + cases.getItemAt(cases.getSelectedIndex()));

    filename = tspfile.toURI().getPath();
    if(filename.charAt(2)==':')
      filename = filename.substring(1);

    for(int i = 0; i<filename.length()-3; i++){
      if(filename.charAt(i)=='%' && filename.charAt(i+1)=='2' && filename.charAt(i+2)=='0')
        filename = filename.substring(0,i) + " " + filename.substring(i+3);
    }

    if (antThread != null) {
      if (antThread.isAlive()) {
        antThread.interrupt();
        try {
          antThread.join();
        } catch (InterruptedException ie) {
          System.err.println(ie);
          return;
        }
        antThread = null;
        startButton.setText("Run selected Algorithm");
        System.gc();
      }
    } else {
      ACOMediator acom = acoMediator();

      if (acom != null) {
        if (antView == null) {
          antView = new AntView(acom);
          new Thread(antView).start();
        } else {
          antView.setAntViewObservable(acom);
        }
        antView.addObserver(this);

        antThread = new Thread(acom.getEnvironment());
        acom.getEnvironment().addObserver(antView);
        acom.getEnvironment().addObserver(this);
        antThread.start();

        startButton.setText("Stop Computation");
      }
    }

  }

  /**ItemListener for algorithm selection*/
  public void itemStateChanged(ItemEvent e) {
    choice = algorithms.getSelectedIndex();
    jp2.removeAll();
    int lines=2;
    switch(choice) {
      case 0:
        for(int j=0; j<3; j++) {
          jp2.add(labels[j]);
          jp2.add(values[j]);
        }

        jp2.add(labels[7]);
        jp2.add(values[7]);
        jp2.add(labels[8]);
        jp2.add(values[8]);
        break;
      case 1:
        for(int j=0; j<3; j++) {
          jp2.add(labels[j]);
          jp2.add(values[j]);
        }

        for(int l=6; l<9; l++) {
          jp2.add(labels[l]);
          jp2.add(values[l]);
        }

        lines = 3;
        break;
      case 2:
        for(int j=0; j<3; j++) {
          jp2.add(labels[j]);
          jp2.add(values[j]);
        }

        jp2.add(labels[5]);
        jp2.add(values[5]);
        jp2.add(labels[7]);
        jp2.add(values[7]);
        jp2.add(labels[8]);
        jp2.add(values[8]);
        lines = 3;
        break;
      case 3:

        for(int j=1; j<5; j++) {
          jp2.add(labels[j]);
          jp2.add(values[j]);
        }

        jp2.add(labels[7]);
        jp2.add(values[7]);
        jp2.add(labels[8]);
        jp2.add(values[8]);
        lines = 3;
    }
    if (lines == 2) {
      this.setSize(400,385);
    }
    else {
      this.setSize(400, 400);
    }
    setInitialValues();

    this.paintComponents(this.getGraphics());
  }

  //0 = alpha, 1 = beta, 2 = roh, 3 = q, 4 = cl, 5 = w, 6 = e, 7 = runs, 8 = ants
  //0=Ant System (AS), 1=Elitist Ant System (EAS), 2=Rank-based AS (ASR), 3=Ant Colony System (ACS)
  public void setInitialValues() {
    values[0].setText("1");
    values[1].setText("2");
    if(choice == 2 || choice == 3)
      values[2].setText("0.1");
    else
      values[2].setText("0.5");
    values[3].setText("0.9");
    values[4].setText("15");
    values[5].setText("6");
    values[6].setText("0");
    values[7].setText("10000");
    if(choice == 3)
      values[8].setText("10");
    else
      values[8].setText("0");
  }

  public ACOMediator acoMediator() {
    switch(choice) {
      case 0:
        acom =
          AntFactory.as(filename, alpha, beta, 0, roh, ants, runs);
        break;
      case 1:
        acom =
          AntFactory.eas(filename, alpha, beta, 0, roh, ew, ants, runs);
        break;
      case 2:
        acom =
          AntFactory.asrank(filename, alpha, beta, 0, roh, w, ants, runs);
        break;
      case 3:
        acom =
          AntFactory.acs(filename, beta, q, roh, cl, ants, runs);
        break;
    }

    return acom;
  }

  public void update(Observable o, Object arg) {
    if (o instanceof AntView) {
      antView.deleteObserver(this);
      acom.getEnvironment().deleteObserver(antView);
      antView = null;

      if (antThread != null) {
        antThread.interrupt();
        try {
          antThread.join();
        } catch (InterruptedException ie) {
          System.err.println(ie);
          return;
        }
      }

      antThread = null;
      startButton.setText("Run selected Algorithm");
      System.gc();
    } else if (o instanceof Environment && arg instanceof Integer) {
      if ((Integer)arg == 1) {
        antThread = null;
        startButton.setText("Run selected Algorithm");
        System.gc();
      }
    }
  }

  public static void main(String[] args) {
    new TspAcoGui();
  }

}
