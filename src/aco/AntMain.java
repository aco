package aco;

import java.io.IOException;

import aco.antview.*;
import aco.graph.*;
import aco.graph.data.*;
import aco.strategy.*;
import aco.strategy.acs.*;
import aco.strategy.as.*;
import aco.strategy.as.eas.*;
import aco.strategy.as.asrank.*;
import aco.strategy.as.simpleas.*;
import aco.mediator.acs.*;
import aco.mediator.as.eas.*;
import aco.mediator.as.asrank.*;
import aco.mediator.as.simpleas.*;
import aco.parameter.acs.*;
import aco.parameter.as.eas.*;
import aco.parameter.as.asrank.*;
import aco.parameter.as.simpleas.*;
import aco.environment.as.*;
import aco.environment.acs.*;
import aco.tsplibreader.*;

public class AntMain {

  public static void runACSAlgorithm(String filename,
      double beta,
      double qzero,
      double roh,
      int cl,
      int numofants,
      int runs) {

    ACSMediator acom = new ACSMediator(new ACSParameter());
    ACOGraph acog = new ACOGraph(acom);
    acom.setACOGraph(acog);

    /* these are the most important */
    acom.setACOStrategy(new ACSStrategy(acom));
    acom.setTauZeroStrategy(new ACSTauZeroStrategy(acom));
    acom.setPheromoneStrategy(new ACSPheromoneStrategy(acom));
    acom.setChoiceInformationStrategy(new ACSChoiceInformationStrategy(acom));

    /* these are not very likely to change */
    acom.setGraphStrategy(new GraphStrategy(acom));
    acom.setHeuristicInformationStrategy(new HeuristicInformationStrategy(acom));

    acom.setGlobalBestTourMapSize(1);
    acom.setMaxNumOfTours(runs);
    acom.setBeta(beta);
    acom.setRoh(roh);
    acom.setQZero(qzero);
    acom.setNearestNeighbourListDepth(cl);

    try {
      TSPLibReader tsplr = new TSPLibReader(filename);
      CoordinateData coordinates = new CoordinateData(tsplr);

      acom.setDistanceStrategy(DistanceStrategy.getStrategyFromTSPLibReader(tsplr, acom));
      acog.initializeFromCoordinates(coordinates);

    } catch (IOException e) {
      System.out.println(e.toString() + "(" + e.getClass() + "): " + e);
      return;
    }

    /* after the graph is initialized we can set the Number of Ants
     * or leave it to the method parameter */
    if (numofants == 0) {
      acom.setNumOfAnts(acom.getNumOfCities());
    } else {
      acom.setNumOfAnts(numofants);
    }
    /* after the graph is initialized we can compute TauZero.. */
    acom.computeTauZero();
    /* ..then we can set the initial pheromone value.. */
    acom.setInitialPheromones(acom.getTauZero());
    /* ..and finally compute the initial Choice Information */
    acom.computeChoiceInformation();

    System.out.println(acom.getACOParameter());

    ACSEnvironment env = new ACSEnvironment(acom);
    acom.setEnvironment(env);

    AntView av = new AntView(acom);
    new Thread(av).start();
    env.addObserver(av);

    env.run();
  }

  public static void runASAlgorithm(String filename,
      double alpha,
      double beta,
      double tauzero,
      double roh,
      int numofants,
      int runs) {
    SimpleASMediator acom = new SimpleASMediator(new SimpleASParameter());
    ACOGraph acog = new ACOGraph(acom);
    acom.setACOGraph(acog);

    /* these are the most important */
    acom.setACOStrategy(new ASStrategy(acom));
    acom.setTauZeroStrategy(new SimpleASTauZeroStrategy(acom));
    acom.setPheromoneStrategy(new SimpleASPheromoneStrategy(acom));
    acom.setChoiceInformationStrategy(new ASChoiceInformationStrategy(acom));

    /* these are not very likely to change */
    acom.setGraphStrategy(new GraphStrategy(acom));
    acom.setHeuristicInformationStrategy(new HeuristicInformationStrategy(acom));

    acom.setGlobalBestTourMapSize(1);
    acom.setMaxNumOfTours(runs);
    acom.setAlpha(alpha);
    acom.setBeta(beta);
    acom.setRoh(roh);

    try {
      TSPLibReader tsplr = new TSPLibReader(filename);
      CoordinateData coordinates = new CoordinateData(tsplr);

      acom.setDistanceStrategy(DistanceStrategy.getStrategyFromTSPLibReader(tsplr, acom));
      acog.initializeFromCoordinates(coordinates);

    } catch (IOException e) {
      System.out.println(e.toString() + "(" + e.getClass() + "): " + e);
      return;
    }

    /* after the graph is initialized we can set the Number of Ants
     * or leave it to the method parameter */
    if (numofants == 0) {
      acom.setNumOfAnts(acom.getNumOfCities());
    } else {
      acom.setNumOfAnts(numofants);
    }
    /* after the graph is initialized we can compute TauZero.. */
    acom.computeTauZero();
    /* ..then we can set the initial pheromone value.. */
    acom.setInitialPheromones(acom.getTauZero());
    /* ..and finally compute the initial Choice Information */
    acom.computeChoiceInformation();

    System.out.println(acom.getACOParameter());

    ASEnvironment env = new ASEnvironment(acom);
    acom.setEnvironment(env);

    AntView av = new AntView(acom);
    new Thread(av).start();
    env.addObserver(av);

    env.run();
  }

  public static void runEASAlgorithm(String filename,
      double alpha,
      double beta,
      double tauzero,
      double roh,
      double e,
      int numofants,
      int runs) {
    EASMediator acom = new EASMediator(new EASParameter());
    ACOGraph acog = new ACOGraph(acom);
    acom.setACOGraph(acog);

    /* these are the most important */
    acom.setACOStrategy(new ASStrategy(acom));
    acom.setTauZeroStrategy(new EASTauZeroStrategy(acom));
    acom.setPheromoneStrategy(new EASPheromoneStrategy(acom));
    acom.setChoiceInformationStrategy(new ASChoiceInformationStrategy(acom));

    /* these are not very likely to change */
    acom.setGraphStrategy(new GraphStrategy(acom));
    acom.setHeuristicInformationStrategy(new HeuristicInformationStrategy(acom));

    acom.setGlobalBestTourMapSize(1);
    acom.setMaxNumOfTours(runs);
    acom.setAlpha(alpha);
    acom.setBeta(beta);
    acom.setRoh(roh);

    try {
      TSPLibReader tsplr = new TSPLibReader(filename);
      CoordinateData coordinates = new CoordinateData(tsplr);

      acom.setDistanceStrategy(DistanceStrategy.getStrategyFromTSPLibReader(tsplr, acom));
      acog.initializeFromCoordinates(coordinates);

    } catch (IOException exception) {
      System.out.println(exception.toString() +
          "(" + exception.getClass() + "): " + exception);
      return;
    }

    if (e == 0.0) {
      acom.setE((double)acom.getNumOfCities());
    } else {
      acom.setE(e);
    }

    /* after the graph is initialized we can set the Number of Ants
     * or leave it to the method parameter */
    if (numofants == 0) {
      acom.setNumOfAnts(acom.getNumOfCities());
    } else {
      acom.setNumOfAnts(numofants);
    }

    /* after the graph is initialized we can compute TauZero.. */
    acom.computeTauZero();
    /* ..then we can set the initial pheromone value.. */
    acom.setInitialPheromones(acom.getTauZero());
    /* ..and finally compute the initial Choice Information */
    acom.computeChoiceInformation();

    System.out.println(acom.getACOParameter());

    ASEnvironment env = new ASEnvironment(acom);
    acom.setEnvironment(env);

    AntView av = new AntView(acom);
    new Thread(av).start();
    env.addObserver(av);

    env.run();
  }

  public static void runASRankAlgorithm(String filename,
      double alpha,
      double beta,
      double tauzero,
      double roh,
      double w,
      int numofants,
      int runs) {
    ASRankMediator acom = new ASRankMediator(new ASRankParameter());
    ACOGraph acog = new ACOGraph(acom);
    acom.setACOGraph(acog);

    /* these are the most important */
    acom.setACOStrategy(new ASStrategy(acom));

    acom.setTauZeroStrategy(new ASRankTauZeroStrategy(acom));
    acom.setPheromoneStrategy(new ASRankPheromoneStrategy(acom));

    /* these are not very likely to change */
    acom.setGraphStrategy(new GraphStrategy(acom));
    acom.setChoiceInformationStrategy(new ASChoiceInformationStrategy(acom));
    acom.setHeuristicInformationStrategy(new HeuristicInformationStrategy(acom));

    acom.setGlobalBestTourMapSize(1);
    acom.setMaxNumOfTours(runs);
    acom.setAlpha(alpha);
    acom.setBeta(beta);
    acom.setRoh(roh);

    try {
      TSPLibReader tsplr = new TSPLibReader(filename);
      CoordinateData coordinates = new CoordinateData(tsplr);

      acom.setDistanceStrategy(DistanceStrategy.getStrategyFromTSPLibReader(tsplr, acom));
      acog.initializeFromCoordinates(coordinates);

    } catch (IOException e) {
      System.out.println(e.toString() + "(" + e.getClass() + "): " + e);
      return;
    }

    if (w == 0.0) {
      acom.setW((double)acom.getNumOfCities());
    } else {
      acom.setW(w);
    }

    /* after the graph is initialized we can set the Number of Ants
     * or leave it to the method parameter */
    if (numofants == 0) {
      acom.setNumOfAnts(acom.getNumOfCities());
    } else {
      acom.setNumOfAnts(numofants);
    }

    /* after the graph is initialized we can compute TauZero.. */
    acom.computeTauZero();
    /* ..then we can set the initial pheromone value.. */
    acom.setInitialPheromones(acom.getTauZero());
    /* ..and finally compute the initial Choice Information */
    acom.computeChoiceInformation();

    System.out.println(acom.getACOParameter());

    ASEnvironment env = new ASEnvironment(acom);
    acom.setEnvironment(env);

    AntView av = new AntView(acom);
    new Thread(av).start();
    env.addObserver(av);

    env.run();
  }

  public static void main(String[] args) {

    String[] algorithms = { "as", "eas", "asrank", "acs" };

    for (int i = 0; i < algorithms.length; i++) {
      if (args[0].equalsIgnoreCase(algorithms[i])) {
        switch (i) {
          case 0:
            /* filename, alpha, beta, tauzero, roh, numofants, iterations */
            runASAlgorithm(args[2], 1.0, 2.0, 0, 0.5, 0, Integer.parseInt(args[1]));
            break;
          case 1:
            /* filename, alpha, beta, tauzero, roh, e, numofants, iterations */
            runEASAlgorithm(args[2], 1.0, 2.0, 0, 0.5, 0, 0, Integer.parseInt(args[1]));
            break;
          case 2:
            /* filename, alpha, beta, tauzero, roh, w, numofants, iterations */
            runASRankAlgorithm(args[2], 1.0, 2.0, 0, 0.5, 6.0, 0, Integer.parseInt(args[1]));
            break;
          case 3:
            /* filename, beta, qzero, roh, cl, numofants, runs */
            runACSAlgorithm(args[2], 2.0, 0.7, 0.1, 15, 15, Integer.parseInt(args[1]));
            break;
        }
      }
    }

    System.out.print("Computation finished..");
    /* wait for a keypress before exit */
    try {
      System.in.read();

    } catch (IOException e) {
      System.out.println(e.toString() + "(" + e.getClass() + "): " + e);
    }

    System.exit(0);
  }

}
