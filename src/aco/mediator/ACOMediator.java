package aco.mediator;

import java.util.HashMap;
import java.util.SortedMap;

import aco.ant.*;
import aco.misc.*;
import aco.graph.*;
import aco.antview.*;
import aco.strategy.*;
import aco.parameter.*;
import aco.environment.*;

public abstract class ACOMediator
  implements AntViewObservable {

  protected ACOGraph acog;
  protected ACOParameter acop;
  protected Environment env;

  public ACOMediator() {
    this.acog = null;
    this.acop = null;
    this.env = null;
  }

  public ACOMediator(ACOParameter acop) {
    this.acog = null;
    this.acop = acop;
    this.env = null;
  }

  public ACOMediator(ACOGraph acog, ACOParameter acop) {
    this.acog = acog;
    this.acop = acop;
    this.env = null;
  }

  public ACOMediator(ACOGraph acog, ACOParameter acop, Environment env) {
    this.acog = acog;
    this.acop = acop;
    this.env = env;
  }

  public ACOGraph getACOGraph() {
    return acog;
  }

  public void setACOGraph(ACOGraph acog) {
    this.acog = acog;
  }

  public ACOParameter getACOParameter() {
    return acop;
  }

  public void setACOParameter(ACOParameter acop) {
    this.acop = acop;
  }

  public Environment getEnvironment() {
    return env;
  }

  public void setEnvironment(Environment env) {
    this.env = env;
  }

  public ACOStrategy getACOStrategy() {
    return acop.getACOStrategy();
  }

  public void setACOStrategy(ACOStrategy acos) {
    acop.setACOStrategy(acos);
  }

  public GraphStrategy getGraphStrategy() {
    return acop.getGraphStrategy();
  }

  public void setGraphStrategy(GraphStrategy gs) {
    acop.setGraphStrategy(gs);
  }

  public TauZeroStrategy getTauZeroStrategy() {
    return acop.getTauZeroStrategy();
  }

  public void setTauZeroStrategy(TauZeroStrategy tzs) {
    acop.setTauZeroStrategy(tzs);
  }

  public DistanceStrategy getDistanceStrategy() {
    return acop.getDistanceStrategy();
  }

  public void setDistanceStrategy(DistanceStrategy ds) {
    acop.setDistanceStrategy(ds);
  }

  public PheromoneStrategy getPheromoneStrategy() {
    return acop.getPheromoneStrategy();
  }

  public void setPheromoneStrategy(PheromoneStrategy ps) {
    acop.setPheromoneStrategy(ps);
  }

  public ChoiceInformationStrategy getChoiceInformationStrategy() {
    return acop.getChoiceInformationStrategy();
  }

  public void setChoiceInformationStrategy(ChoiceInformationStrategy cis) {
    acop.setChoiceInformationStrategy(cis);
  }

  public HeuristicInformationStrategy getHeuristicInformationStrategy() {
    return acop.getHeuristicInformationStrategy();
  }

  public void setHeuristicInformationStrategy(HeuristicInformationStrategy his) {
    acop.setHeuristicInformationStrategy(his);
  }

  public int getInfinity() {
    return acop.getInfinity();
  }

  public double getBeta() {
    return acop.getBeta();
  }

  public void setBeta(double Beta) {
    acop.setBeta(Beta);
  }

  public double getRoh() {
    return acop.getRoh();
  }

  public void setRoh(double Roh) {
    acop.setRoh(Roh);
  }

  public double getTauZero() {
    return acop.getTauZero();
  }

  public void setTauZero(double TauZero) {
    acop.setTauZero(TauZero);
  }
    
  public int getMaxNumOfTours() {
    return acop.getMaxNumOfTours();
  }

  public void setMaxNumOfTours(int v) {
    acop.setMaxNumOfTours(v);
  }

  public int getNumOfAnts() {
    return acop.getNumOfAnts();
  }

  public void setNumOfAnts(int v) {
    acop.setNumOfAnts(v);
  }

  public int getNumOfCities() {
    return acop.getNumOfCities();
  }

  public void setNumOfCities(int v) {
    acop.setNumOfCities(v);
  }

  public int getNearestNeighbourListDepth() {
    return acop.getNearestNeighbourListDepth();
  }

  public void setNearestNeighbourListDepth(int NearestNeighbourListDepth) {
    acop.setNearestNeighbourListDepth(NearestNeighbourListDepth);
  }

  public int getNearestNeighbour(int x, int y) {
    return acog.getNearestNeighbour(x, y);
  }

  public int getDistance(int x, int y) {
    return acog.getDistance(x, y);
  }

  public void setDistance(int x, int y, int v) {
    acog.setDistance(x, y, v);
  }

  public CoordinatePair<Integer, Integer> getCoordinates(int City) {
    return acog.getCoordinates(City);
  }

  public HashMap<Integer, CoordinatePair<Integer, Integer>> getCoordinateData() {
    return acog.getCoordinateData();
  }

  public double getPheromone(int x, int y) {
    return acog.getPheromone(x, y);
  }

  public void setPheromone(int x, int y, double v) {
    acog.setPheromone(x, y, v);
  }

  public double getChoiceInformation(int x, int y) {
    return acog.getChoiceInformation(x, y);
  }

  public void setChoiceInformation(int x, int y, double v) {
    acog.setChoiceInformation(x, y, v);
  }

  public double getHeuristicInformation(int x, int y) {
    return acog.getHeuristicInformation(x, y);
  }
  
  public void setHeuristicInformation(int x, int y, double v) {
    acog.setHeuristicInformation(x, y, v);
  }
  
  public void computeTauZero() {
    acop.getTauZeroStrategy().computeTauZero();
  }

  public void computeChoiceInformation() {
    acop.getChoiceInformationStrategy().computeChoiceInformation();
  }

  public void computeHeuristicInformation() {
    acop.getHeuristicInformationStrategy().computeHeuristicInformation();
  }

  public void computeNearestNeighbourListDepth() {
    acop.getGraphStrategy().computeNearestNeighbourListDepth();
  }

  public int[][] computeDistances() {
    return acop.getDistanceStrategy().computeDistances();
  }

  public void setInitialPheromones(double TauZero) {
    acop.getPheromoneStrategy().setInitialPheromones(TauZero);
  }

  public int nearestNeighbourHeuristicRandomStart() {
    return acop.getGraphStrategy().nearestNeighbourHeuristicRandomStart();
  }

  public void pheromoneUpdate(Ant[] ants) {
    acop.getPheromoneStrategy().pheromoneUpdate(ants);
  }

  public void computeTourLength(Ant ant) {
    acop.getACOStrategy().computeTourLength(ant);
  }

  public void pickInitialRandomCity(Ant ant) {
    acop.getACOStrategy().pickInitialRandomCity(ant);
  }

  public void ACODecisionRule(Ant ant, int Step) {
    acop.getACOStrategy().ACODecisionRule(ant, Step);
  }

  public int getGlobalBestTourMapSize() {
    return acop.getGlobalBestTourMapSize();
  }

  public void setGlobalBestTourMapSize(int GlobalBestTourMapSize) {
    acop.setGlobalBestTourMapSize(GlobalBestTourMapSize);
  }

  public SortedMap<Integer, int[]> getGlobalBestTourMap() {
    return env.getGlobalBestTourMap();
  }

  public void addGlobalBestTour(Integer TourLength, int[] Tour) {
    env.addGlobalBestTour(TourLength, Tour);
  }

  public int[] getGlobalBestTour() {
    return env.getGlobalBestTour();
  }

  public int getGlobalBestTour(int index) {
    return env.getGlobalBestTour(index);
  }

  public void setGlobalBestTour(int[] GlobalBestTour) {
    env.setGlobalBestTour(GlobalBestTour);
  }

  public void setGlobalBestTour(int index, int GlobalBestTour) {
    env.setGlobalBestTour(index, GlobalBestTour);
  }

  public int getGlobalBestTourLength() {
    return env.getGlobalBestTourLength();
  }

  public void setGlobalBestTourLength(int GlobalBestTourLength) {
    env.setGlobalBestTourLength(GlobalBestTourLength);
  }

  public int getGlobalBestTourIteration() {
    return env.getGlobalBestTourIteration();
  }

  public void setGlobalBestTourIteration(int GlobalBestTourIteration) {
    env.setGlobalBestTourIteration(GlobalBestTourIteration);
  }

}
