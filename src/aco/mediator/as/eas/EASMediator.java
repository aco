package aco.mediator.as.eas;

import aco.mediator.as.*;
import aco.parameter.as.*;
import aco.parameter.as.eas.*;

public class EASMediator
  extends ASMediator {

  public EASMediator() {
    super();
  }

  public EASMediator(EASParameter easp) {
    super((ASParameter) easp);
  }

  public double getE() {
    return ((EASParameter) acop).getE();
  }

  public void setE(double E) {
    ((EASParameter) acop).setE(E);
  }

}
