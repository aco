package aco.mediator.as.simpleas;

import aco.mediator.as.*;
import aco.parameter.as.*;
import aco.parameter.as.simpleas.*;

public class SimpleASMediator
  extends ASMediator {

  public SimpleASMediator() {
    super();
  }

  public SimpleASMediator(SimpleASParameter sasp) {
    super((ASParameter) sasp);
  }

}
