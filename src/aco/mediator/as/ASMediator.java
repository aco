package aco.mediator.as;

import aco.mediator.*;
import aco.parameter.*;
import aco.parameter.as.*;

public abstract class ASMediator
  extends ACOMediator {

  public ASMediator() {
    super();
  }

  public ASMediator(ASParameter asp) {
    super((ACOParameter) asp);
  }

  public double getAlpha() {
    return ((ASParameter)acop).getAlpha();
  }

  public void setAlpha(double Alpha) {
    ((ASParameter)acop).setAlpha(Alpha);
  }

}
