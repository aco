package aco.mediator.as.asrank;

import aco.mediator.as.*;
import aco.parameter.as.*;
import aco.parameter.as.asrank.*;

public class ASRankMediator
  extends ASMediator {

  public ASRankMediator() {
    super();
  }

  public ASRankMediator(ASRankParameter asrp) {
    super((ASParameter) asrp);
  }

  public double getW() {
    return ((ASRankParameter) acop).getW();
  }

  public void setW(double W) {
    ((ASRankParameter) acop).setW(W);
  }

}
