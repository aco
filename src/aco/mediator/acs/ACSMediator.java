package aco.mediator.acs;

import aco.mediator.*;
import aco.parameter.*;
import aco.parameter.acs.*;

public class ACSMediator
  extends ACOMediator {

  public ACSMediator() {
    super();
  }

  public ACSMediator(ACSParameter acsp) {
    super((ACOParameter) acsp);
  }

  public double getQZero(){
    return ((ACSParameter)acop).getQZero();
  }

  public void setQZero(double QZero){
    ((ACSParameter)acop).setQZero(QZero);
  }

}
