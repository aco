package aco.environment.data;

import java.util.TreeMap;
import java.util.SortedMap;

import aco.mediator.*;

public class EnvironmentData {

  protected ACOMediator acom;

  protected int[] GlobalBestTour;
  protected int GlobalBestTourLength;
  protected int GlobalBestTourIteration;
  protected SortedMap<Integer, int[]> GlobalBestTourMap;

  public EnvironmentData(ACOMediator acom) {
    this.acom = acom;

    this.GlobalBestTour = new int[acom.getNumOfCities() + 1];
    this.GlobalBestTourLength = acom.getInfinity();
    this.GlobalBestTourIteration = 0;
    this.GlobalBestTourMap = new TreeMap<Integer, int[]>();
  }

  public int[] getGlobalBestTour() {
    return this.GlobalBestTour;
  }

  public int getGlobalBestTour(int index) {
    return this.GlobalBestTour[index];
  }

  public void setGlobalBestTour(int[] GlobalBestTour) {
    this.GlobalBestTour = GlobalBestTour;
  }

  public void setGlobalBestTour(int index, int GlobalBestTour) {
    this.GlobalBestTour[index] = GlobalBestTour;
  }

  public int getGlobalBestTourLength() {
    return this.GlobalBestTourLength;
  }

  public void setGlobalBestTourLength(int GlobalBestTourLength) {
    this.GlobalBestTourLength = GlobalBestTourLength;
  }

  public int getGlobalBestTourIteration() {
    return this.GlobalBestTourIteration;
  }

  public void setGlobalBestTourIteration(int GlobalBestTourIteration) {
    this.GlobalBestTourIteration = GlobalBestTourIteration;
  }

  public SortedMap<Integer, int[]> getGlobalBestTourMap() {
    return this.GlobalBestTourMap;
  }

  public void addGlobalBestTour(Integer TourLength, int[] Tour) {
    if (GlobalBestTourMap.size() < acom.getGlobalBestTourMapSize()) {
      GlobalBestTourMap.put(TourLength, Tour);
    } else if ( TourLength < GlobalBestTourMap.lastKey() ) {
      GlobalBestTourMap.remove(GlobalBestTourMap.lastKey());
      GlobalBestTourMap.put(TourLength, Tour);
    }
  }

}
