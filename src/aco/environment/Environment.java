package aco.environment;

import java.lang.Runtime;
import java.util.SortedMap;
import java.util.Observable;

import aco.ant.*;
import aco.misc.*;
import aco.mediator.*;
import aco.environment.data.*;

public abstract class Environment
  extends Observable
  implements Runnable {

  protected Ant[] ants;
  protected ACOMediator acom;
  protected EnvironmentData ed;

  public Environment(ACOMediator acom) {
    this.acom = acom;

    this.ants = new Ant[acom.getNumOfAnts()];

    for (int i = 0; i < acom.getNumOfAnts(); i++)
      ants[i] = new Ant(acom);

    this.ed = new EnvironmentData(acom);

    /* add a shutdown hook to print out the last best solution */
    Runtime.getRuntime().addShutdownHook(new EnvironmentShutdownHook(this));

    /* run the garbage collector after all the initialization is done */
    System.gc();
  }

  /* d'tor */
  protected void finalize() {
    deleteObservers();
  }

  protected abstract void constructSolutions();

  protected abstract boolean terminateCondition(int iteration);

  public void run() {
    int i = 0;
    while ( !terminateCondition(i++) &&
        !interruptCondition() ) {
      constructSolutions();
      updateStatistics(i);
      acom.pheromoneUpdate(ants);
    }
    this.setChanged();
    this.notifyObservers(1);
  }

  protected boolean interruptCondition() {
    return Thread.interrupted();
  }

  protected void updateStatistics(int iteration) {
    for (Ant ant : ants) {

      if (ant.getTourLength() < acom.getGlobalBestTourLength()) {
        acom.setGlobalBestTourLength(ant.getTourLength());
        acom.setGlobalBestTourIteration(iteration);

        for (int i = 0; i < acom.getNumOfCities() + 1; i++) {
          acom.setGlobalBestTour(i, ant.getTour(i));
        }

        acom.addGlobalBestTour((Integer)acom.getGlobalBestTourLength(),
            acom.getGlobalBestTour());

        this.setChanged();
        this.notifyObservers();

        /* since we are already at it.. run the garbage collector */
        System.gc();
      }

    }
  }

  public int[] getGlobalBestTour() {
    return ed.getGlobalBestTour();
  }

  public int getGlobalBestTour(int index) {
    return ed.getGlobalBestTour(index);
  }

  public void setGlobalBestTour(int[] GlobalBestTour) {
    ed.setGlobalBestTour(GlobalBestTour);
  }

  public void setGlobalBestTour(int index, int GlobalBestTour) {
    ed.setGlobalBestTour(index, GlobalBestTour);
  }

  public int getGlobalBestTourLength() {
    return ed.getGlobalBestTourLength();
  }

  public void setGlobalBestTourLength(int GlobalBestTourLength) {
    ed.setGlobalBestTourLength(GlobalBestTourLength);
  }

  public int getGlobalBestTourIteration() {
    return ed.getGlobalBestTourIteration();
  }

  public void setGlobalBestTourIteration(int GlobalBestTourIteration) {
    ed.setGlobalBestTourIteration(GlobalBestTourIteration);
  }

  public SortedMap<Integer, int[]> getGlobalBestTourMap() {
    return ed.getGlobalBestTourMap();
  }

  public void addGlobalBestTour(Integer TourLength, int[] Tour) {
    ed.addGlobalBestTour(TourLength, Tour);
  }

  @Override
    public String toString() {
      StringBuilder result = new StringBuilder();

      result.append("Shortest tour at iteration " +
          acom.getGlobalBestTourIteration() +
          " with length " +
          acom.getGlobalBestTourLength() +
          "\n");

      for (int t : acom.getGlobalBestTour())
        result.append(t + " -- ");

      result.delete(result.lastIndexOf(" -- "), result.length());

      int i = 0;
      for (int t : acom.getGlobalBestTour()) {
        i = 0;
        /* necessary for loop so that we don't check the last city
         * which is equal to the first one */
        for (int c = 0; c < acom.getNumOfCities(); c++)
          if (acom.getGlobalBestTour(c) == t && i++ > 0)
            result.append("\n" + "crossed " + c + " more than once!");
      }

      return result.toString();
    }

}
