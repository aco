package aco.environment.acs;

import aco.ant.*;
import aco.mediator.*;
import aco.mediator.acs.*;
import aco.strategy.acs.*;
import aco.environment.*;

public class ACSEnvironment
  extends Environment {

  public ACSEnvironment(ACSMediator acom) {
    super((ACOMediator)acom);
  }

  protected void constructSolutions() {
    /* clear ants memory and ..
     * assign an initial random city to ant */
    /* TODO: threading */
    for (Ant ant : ants) {
      ant.reset();
      acom.pickInitialRandomCity(ant);
    }

    /* let each ant construct a complete tour 
     * start a 1 since every ant already has an initial city */
    /* TODO: threading */
    for (int step = 1; step < acom.getNumOfCities(); step++) {
      for (Ant ant : ants) {
        acom.ACODecisionRule(ant, step);
        ((ACSPheromoneStrategy)acom.getPheromoneStrategy()).localPheromoneUpdate(ant, step);
      }
    }

    /* compute tour length of each ant */
    /* TODO: threading */
    for (Ant ant : ants) {
      acom.computeTourLength(ant);
    }

  }

  protected boolean terminateCondition(int iteration) {
    /* 1) solution within predefined distance
     * 2) max number of tour constructions or algorithm iterations
     * 3) max cpu time
     * 4) algorithm stagnation behaviour */
    if (iteration >= acom.getMaxNumOfTours()) {
      return true;
    } else {
      return false;
    }

  }

}
