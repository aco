package aco.misc;

import aco.environment.*;

public class EnvironmentShutdownHook extends Thread {

  protected Environment env;

  public EnvironmentShutdownHook(Environment env) {
    this.env = env;
  }

  public void run() {
    System.out.println(env);
  }
}
