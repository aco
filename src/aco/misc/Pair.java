package aco.misc;

public abstract class Pair<F, S> {
  protected final F first;
  protected final S second;

  public Pair(F first, S second) {
    this.first = first;
    this.second = second;
  }

  public F getFirst() {
    return this.first;
  }

  public S getSecond() {
    return this.second;
  }

}
