package aco.misc;

public class CoordinatePair<F, S>
  extends Pair<F, S> {

  public CoordinatePair(F first, S second) {
    super(first, second);
  }

}
