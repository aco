package aco.strategy;

import aco.misc.*;
import aco.mediator.*;
import aco.tsplibreader.*;
import aco.strategy.distance.*;

public abstract class DistanceStrategy {

  protected ACOMediator acom;

  public DistanceStrategy(ACOMediator acom) {
    this.acom = acom;
  }

  public int[][] computeDistances() {

    int[][] Distance = new int[acom.getNumOfCities()][acom.getNumOfCities()];

    for (int k1 : acom.getCoordinateData().keySet()) {
      for (int k2 : acom.getCoordinateData().keySet()) {
        if (k1 == k2)
          Distance[k1][k2] = acom.getInfinity();
        else
          Distance[k1][k2] = computeDistance(acom.getCoordinates(k1), acom.getCoordinates(k2));
      }
    }

    return Distance;
  }

  public abstract int computeDistance(
      CoordinatePair<Integer, Integer> CityOne,
      CoordinatePair<Integer, Integer> CityTwo);

  public static DistanceStrategy getStrategyFromTSPLibReader(TSPLibReader tsplr, ACOMediator acom) {
    switch (tsplr.getAlgorithm()) {
      case 0:
      case 1:
        return new EUC_2D_DistanceStrategy(acom);
      case 2:
        return new ATT_DistanceStrategy(acom);
    }
    return new EUC_2D_DistanceStrategy(acom);
  }

}
