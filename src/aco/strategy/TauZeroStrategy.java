package aco.strategy;

import aco.mediator.*;

public abstract class TauZeroStrategy {

  protected ACOMediator acom;

  public TauZeroStrategy(ACOMediator acom) {
    this.acom = acom;
  }

  public abstract void computeTauZero();

}
