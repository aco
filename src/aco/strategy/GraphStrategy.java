package aco.strategy;

import aco.mediator.*;

public class GraphStrategy {

  protected ACOMediator acom;

  public GraphStrategy(ACOMediator acom) {
    this.acom = acom;
  }

  public void computeNearestNeighbourListDepth() {
    if (acom.getNearestNeighbourListDepth() == 0) {
      if (acom.getNumOfCities() <= 30)
        acom.setNearestNeighbourListDepth(acom.getNumOfCities() - 1);
      else if (acom.getNumOfCities() > 30 && acom.getNumOfCities() < 80)
        acom.setNearestNeighbourListDepth(acom.getNumOfCities()/2);
      else
        acom.setNearestNeighbourListDepth(40);
    } else if (acom.getNearestNeighbourListDepth() > acom.getNumOfCities()) {
      acom.setNearestNeighbourListDepth(acom.getNumOfCities());
    }
  }

  public int nearestNeighbourHeuristicRandomStart() {

    boolean[] visited = new boolean[acom.getNumOfCities()];

    for (int i = 0; i < acom.getNumOfCities(); i++)
      visited[i] = false;

    return nearestNeighbourHeuristic(
        (int)( Math.random() * acom.getNumOfCities() ),
        visited,
        acom.getNumOfCities());
  }

  public int nearestNeighbourHeuristic(int city, boolean[] visited, int remaining) {

    int nextmin = acom.getInfinity();
    int nextcity = 0;

    for (int i = 0; i < acom.getNumOfCities(); i++) {
      if ( (i != city) &&
          (! visited[i]) &&
          (acom.getDistance(i, city) < nextmin) ) {
        nextmin = acom.getDistance(i, city);
        nextcity = i;
          }
    }

    visited[nextcity] = true;

    if (remaining == 1)
      return nextmin;
    else
      return nextmin + nearestNeighbourHeuristic(nextcity, visited, remaining - 1);
  }

}
