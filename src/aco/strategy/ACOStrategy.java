package aco.strategy;

import aco.ant.*;
import aco.mediator.*;

public abstract class ACOStrategy {

  protected ACOMediator acom;

  public ACOStrategy(ACOMediator acom) {
    this.acom = acom;
  }

  public void computeTourLength(Ant ant) {
    ant.setTour(acom.getNumOfCities(), ant.getTour(0));
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      /* tourlength = tourlength + distance(i, i+1) */
      ant.setTourLength(
          ant.getTourLength() +
          acom.getDistance(ant.getTour(i), ant.getTour(i+1)));
    }
  }

  public void pickInitialRandomCity(Ant ant) {
    int c = (int)(Math.random() * (double)acom.getNumOfCities());
    ant.setTour(0, c);
    ant.setVisited(c, true);
  }

  public abstract void ACODecisionRule(Ant ant, int Step);

  protected void chooseBestNext(Ant ant, int Step) {
    int nc = 0;
    double v = 0.0;
    int CurrentCity = ant.getTour(Step - 1);

    for (int j = 0; j < acom.getNumOfCities(); j++) {
      if (! ant.getVisited(j)) {
        if (acom.getChoiceInformation(CurrentCity,j) > v) {
          nc = j;
          v = acom.getChoiceInformation(CurrentCity,j);
        }
      }
    }

    ant.setTour(Step, nc);
    ant.setVisited(nc, true);
  }

}
