package aco.strategy;

import aco.mediator.*;

public class HeuristicInformationStrategy {

  protected double[][] heuristicInformation;

  protected ACOMediator acom;

  public HeuristicInformationStrategy(ACOMediator acom) {
    this.acom = acom;
  }

  public void computeHeuristicInformation() {
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      for (int j = 0; j < acom.getNumOfCities(); j++) {
        acom.setHeuristicInformation(i,j,
            (1.0/(double)acom.getDistance(i, j) ));
      }
    }
  }

}
