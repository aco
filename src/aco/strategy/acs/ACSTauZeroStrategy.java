package aco.strategy.acs;

import aco.strategy.*;
import aco.mediator.*;
import aco.mediator.acs.*;

public class ACSTauZeroStrategy extends TauZeroStrategy {

  public ACSTauZeroStrategy(ACSMediator acom) {
    super((ACOMediator) acom);
  }

  public void computeTauZero() {
    acom.setTauZero(
        1.0 /
        ( (double)(acom.getNumOfCities() *
                   acom.nearestNeighbourHeuristicRandomStart())) );
  }

}
