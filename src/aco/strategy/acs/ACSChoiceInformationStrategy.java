package aco.strategy.acs;

import aco.strategy.*;
import aco.mediator.*;
import aco.mediator.acs.*;

public class ACSChoiceInformationStrategy
  extends ChoiceInformationStrategy {

  public ACSChoiceInformationStrategy(ACSMediator acom) {
    super((ACOMediator) acom);
  }

  public void computeChoiceInformation() {
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      for (int j = 0; j < acom.getNumOfCities(); j++) {
        acom.setChoiceInformation(i,j,
          (double)acom.getPheromone(i,j) *
          Math.pow( acom.getHeuristicInformation(i,j) , acom.getBeta() ));
      }
    }
  }

}
