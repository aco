package aco.strategy.acs;

import aco.ant.*;
import aco.strategy.*;
import aco.mediator.*;
import aco.mediator.acs.*;

public class ACSStrategy extends ACOStrategy{

  public ACSStrategy(ACSMediator acom) {
    super((ACOMediator) acom);
  }

  public void ACODecisionRule(Ant ant, int Step){
    double q = Math.random();//random.nextDouble();
    if(q < ((ACSMediator)acom).getQZero())
      chooseBestNext(ant, Step);
    else
      neighourListDecisionRule(ant, Step);
  }

  protected void neighourListDecisionRule(Ant ant, int i) {
    int c = ant.getTour(i-1); //index of current city of ant k
    double sum_probabilities = 0.0;
    double divisor = 0.0;  //new
    double r;
    double[] selection_probability = new double[acom.getNearestNeighbourListDepth()];

    for (int j=0; j < acom.getNearestNeighbourListDepth(); j++) {

      if (ant.getVisited(acom.getNearestNeighbour(c, j))) {
        selection_probability[j] = 0.0;
      } else {
        /* g.choice_info[c][g.nn_list[c][j]]; */
        selection_probability [j] = acom.getChoiceInformation(c, acom.getNearestNeighbour(c, j));
        sum_probabilities = sum_probabilities + selection_probability [j];
        /* g.choice_info[c][g.nn_list[c][j]]; */
        divisor += acom.getChoiceInformation(c, acom.getNearestNeighbour(c, j));
      }

    }

    //=>no city on neighbor list
    if (sum_probabilities == 0.0) {
      chooseBestNext(ant,i);
    } else {

      r = Math.random(); //random.nextInt((int)(sum_probabilities + 1)); //?????Math.random() * sum_probabilities;
      int j = 0;
      double p = selection_probability[j]/divisor;    //selection_probability[j];  : new !!!

      while (p < r) {
        j=j+1;
        p = p + selection_probability[j]/divisor;  //p + selection_probability[j];   : new !!!!!!!!!
      }

      ant.setTour(i, acom.getNearestNeighbour(c, j));//ant[k].tour[i] = g.nn_list[c][j];
      ant.setVisited(acom.getNearestNeighbour(c, j), true);
    }

  }

}
