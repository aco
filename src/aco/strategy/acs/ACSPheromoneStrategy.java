package aco.strategy.acs;

import aco.ant.*;
import aco.strategy.*;
import aco.mediator.*;
import aco.mediator.acs.*;

public class ACSPheromoneStrategy
  extends PheromoneStrategy {

  public ACSPheromoneStrategy(ACSMediator acom) {
    super((ACOMediator) acom);
  }

  //global Pheromone Update
  public void pheromoneUpdate(Ant[] ants) {
    double deltaTau = 1.0 / (double)acom.getGlobalBestTourLength();
    int j;
    int l;
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      j = acom.getGlobalBestTour(i);
      l = acom.getGlobalBestTour(i+1);
      acom.setPheromone(j, l,
          ( (1 - acom.getRoh()) * acom.getPheromone(j, l) ) +
          (acom.getRoh() * deltaTau) );

      //g.pheromones[l][j] = g.pheromones[j][l];
      acom.setChoiceInformation(j, l,
          acom.getPheromone(j, l) *
          Math.pow((1.0/  acom.getDistance(j, l) ), acom.getBeta()));

      //g.choice_info[j][l] =  g.pheromones[j][l] *
      //Math.pow((1.0/g.distance_matrix[j][l]), beta);
      //g.choice_info[l][j] = g.choice_info[l][j];
    }
  }

  //local Pheromone Update
  public void localPheromoneUpdate(Ant k, int i) {
    int j = k.getTour(i-1);
    int l = k.getTour(i);

    acom.setPheromone(j, l,
        ( (1 - acom.getRoh()) * acom.getPheromone(j, l) ) +
        ( acom.getRoh() * acom.getTauZero() ));

    //g.pheromones[l][j] = g.pheromones[j][l];
    acom.setChoiceInformation(j, l,
        acom.getPheromone(j, l) *
        Math.pow( (1.0/acom.getDistance(j, l)), acom.getBeta() ) );
  }

}
