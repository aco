package aco.strategy.distance;

import aco.misc.*;
import aco.strategy.*;
import aco.mediator.*;

public class ATT_DistanceStrategy
  extends DistanceStrategy {

  public ATT_DistanceStrategy(ACOMediator acom) {
    super(acom);
  }

  public int computeDistance(
      CoordinatePair<Integer, Integer> CityOne,
      CoordinatePair<Integer, Integer> CityTwo) {
    /* ATT */
    double xd = (double)(CityOne.getFirst() - CityTwo.getFirst());
    double yd = (double)(CityOne.getSecond() - CityTwo.getSecond());
    double rij = Math.sqrt( (xd*xd + yd*yd) / 10.0 );
    return (int)Math.round(rij);
  }

}
