package aco.strategy.distance;

import aco.misc.*;
import aco.strategy.*;
import aco.mediator.*;

public class EUC_2D_DistanceStrategy
  extends DistanceStrategy {

  public EUC_2D_DistanceStrategy(ACOMediator acom) {
    super(acom);
  }

  public int computeDistance(
      CoordinatePair<Integer, Integer> CityOne,
      CoordinatePair<Integer, Integer> CityTwo) {
    /* pythagoras: a^2 + b^2 = c^2 => c = sqrt( a^2 + b^2 ) */
    return (int)Math.round(
        Math.sqrt(
        Math.pow((double)(CityOne.getFirst() - CityTwo.getFirst()), 2) +
        Math.pow( (double)(CityOne.getSecond() - CityTwo.getSecond()), 2 ) ));
  }

}
