package aco.strategy;

import aco.mediator.*;

public abstract class ChoiceInformationStrategy {

  protected ACOMediator acom;

  public ChoiceInformationStrategy(ACOMediator acom) {
    this.acom = acom;
  }

  public abstract void computeChoiceInformation();

}
