package aco.strategy.as;

import aco.ant.*;
import aco.strategy.*;
import aco.mediator.*;
import aco.mediator.as.*;

public class ASStrategy
  extends ACOStrategy {

  public ASStrategy(ASMediator acom) {
    super((ACOMediator) acom);
  }

  public void ACODecisionRule(Ant ant, int Step) {

    int CurrentCity = ant.getTour(Step - 1);

    double SumProbabilities = 0.0;
    double SelectionProbability[] = new double[acom.getNearestNeighbourListDepth()];

    for (int j = 0; j < acom.getNearestNeighbourListDepth(); j++) {

      if ( ant.getVisited( acom.getNearestNeighbour(CurrentCity,j) ) ) {
        SelectionProbability[j] = 0.0;
      } else {

        SelectionProbability[j] =
          acom.getChoiceInformation( CurrentCity,
              acom.getNearestNeighbour(CurrentCity,j) );
        SumProbabilities += SelectionProbability[j];

      }

    }

    if (SumProbabilities == 0.0) {
      chooseBestNext(ant, Step);
    } else {

      int j = 0;
      double r = Math.random() * SumProbabilities;
      double p = SelectionProbability[j];

      while (p < r) {
        p += SelectionProbability[++j];
      }

      ant.setTour(Step, acom.getNearestNeighbour(CurrentCity,j));
      ant.setVisited( acom.getNearestNeighbour(CurrentCity,j), true);
    }

  }

}
