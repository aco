package aco.strategy.as;

import aco.ant.*;
import aco.strategy.*;
import aco.mediator.*;
import aco.mediator.as.*;

public abstract class ASPheromoneStrategy
  extends PheromoneStrategy {

  public ASPheromoneStrategy(ASMediator acom) {
    super((ACOMediator) acom);
  }

  public abstract void pheromoneUpdate(Ant[] ants);

  protected abstract void depositPheromone(Ant ant);

  protected void evaporate() {
    for (int i = 1; i < acom.getNumOfCities(); i++) {
      for (int j = 1; j < acom.getNumOfCities(); j++) {
        acom.setPheromone(i, j,
            (1.0 - acom.getRoh()) * acom.getPheromone(i, j) );
      }
    }
  }

}
