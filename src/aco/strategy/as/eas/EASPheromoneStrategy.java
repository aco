package aco.strategy.as.eas;

import aco.ant.*;
import aco.strategy.as.*;
import aco.mediator.as.*;
import aco.mediator.as.eas.*;

public class EASPheromoneStrategy
  extends ASPheromoneStrategy {

  public EASPheromoneStrategy(EASMediator acom) {
    super((ASMediator) acom);
  }

  public void pheromoneUpdate(Ant[] ants) {
    this.evaporate();

    for (Ant ant : ants) {
      this.depositPheromone(ant);
    }

    enforceGlobalBest();

    acom.computeChoiceInformation();
  }

  protected void enforceGlobalBest() {
    /* enforcement of global best route */
    int j = 0, l = 0;
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      j = acom.getGlobalBestTour(i);
      l = acom.getGlobalBestTour(i+1);
      acom.setPheromone( j, l, (acom.getPheromone(j,l) +
            ((EASMediator)acom).getE()/(double)acom.getGlobalBestTourLength()) );
    }
  }

  protected void depositPheromone(Ant ant) {
    double dt = 1.0/(double)ant.getTourLength();
    int j = 0;
    int l = 0;
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      j = ant.getTour(i);
      l = ant.getTour(i+1);
      /* setPheromone will update j,l and l,j */
      acom.setPheromone( j, l, (acom.getPheromone(j,l) + dt) );
    }
  }

}
