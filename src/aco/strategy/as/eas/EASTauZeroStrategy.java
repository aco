package aco.strategy.as.eas;

import aco.strategy.*;
import aco.mediator.as.*;
import aco.mediator.as.eas.*;

public class EASTauZeroStrategy
  extends TauZeroStrategy {

  public EASTauZeroStrategy(EASMediator acom) {
    super((ASMediator) acom);
  }

  public void computeTauZero() {
    acom.setTauZero(
        (((EASMediator)acom).getE() + (double)(acom.getNumOfAnts())) /
      (acom.getRoh() * (double)acom.nearestNeighbourHeuristicRandomStart() ));
  }

}
