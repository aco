package aco.strategy.as.simpleas;

import aco.ant.*;
import aco.strategy.as.*;
import aco.mediator.as.*;
import aco.mediator.as.simpleas.*;

public class SimpleASPheromoneStrategy
  extends ASPheromoneStrategy {

  public SimpleASPheromoneStrategy(SimpleASMediator acom) {
    super((ASMediator) acom);
  }

  public void pheromoneUpdate(Ant[] ants) {
    this.evaporate();

    for (Ant ant : ants) {
      this.depositPheromone(ant);
    }

    acom.computeChoiceInformation();
  }

  protected void depositPheromone(Ant ant) {
    double dt = 1.0/ant.getTourLength();
    int j = 0;
    int l = 0;
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      j = ant.getTour(i);
      l = ant.getTour(i+1);
      /* setPheromone will update j,l and l,j */
      acom.setPheromone( j, l, (acom.getPheromone(j,l) + dt) );
    }
  }

}
