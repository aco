package aco.strategy.as.simpleas;

import aco.strategy.*;
import aco.mediator.as.*;
import aco.mediator.as.simpleas.*;

public class SimpleASTauZeroStrategy
  extends TauZeroStrategy {

  public SimpleASTauZeroStrategy(SimpleASMediator acom) {
    super((ASMediator) acom);
  }

  public void computeTauZero() {
    acom.setTauZero(
      (double)acom.getNumOfAnts() /
      (double)acom.nearestNeighbourHeuristicRandomStart() );
  }

}
