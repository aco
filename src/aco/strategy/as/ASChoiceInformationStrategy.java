package aco.strategy.as;

import aco.strategy.*;
import aco.mediator.*;
import aco.mediator.as.*;

public class ASChoiceInformationStrategy
  extends ChoiceInformationStrategy {

  public ASChoiceInformationStrategy(ASMediator acom) {
    super((ACOMediator) acom);
  }

  public void computeChoiceInformation() {
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      for (int j = 0; j < acom.getNumOfCities(); j++) {
        acom.setChoiceInformation(i,j,
          Math.pow( acom.getPheromone(i,j), ((ASMediator)acom).getAlpha() ) *
          Math.pow( acom.getHeuristicInformation(i,j) , acom.getBeta() ));
      }
    }
  }

}
