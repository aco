package aco.strategy.as.asrank;

import java.util.TreeMap;
import java.util.SortedMap;

import aco.ant.*;
import aco.strategy.as.*;
import aco.mediator.as.*;
import aco.mediator.as.asrank.*;

public class ASRankPheromoneStrategy
  extends ASPheromoneStrategy {

  public ASRankPheromoneStrategy(ASRankMediator acom) {
    super((ASMediator)acom);
  }

  public void pheromoneUpdate(Ant[] ants) {
    this.evaporate();

    this.applyRankPheromones(ants);
    this.enforceGlobalBest();

    acom.computeChoiceInformation();
  }

  protected void applyRankPheromones(Ant[] ants) {
    SortedMap<Integer, Ant> antmap = new TreeMap<Integer, Ant>();

    int max = 0, slots = 0;
    for (Ant ant : ants) {
      if (ant.getTourLength() == acom.getGlobalBestTourLength())
        continue;
      if (slots++ < (int)((ASRankMediator)acom).getW()) {
        antmap.put(ant.getTourLength(), ant);
      } else if (ant.getTourLength() < antmap.lastKey()) {
        for (int i : antmap.keySet()) {
          if (i > max)
            max = i;
        }
        antmap.remove(max);
        antmap.put(ant.getTourLength(), ant);
        max = 0;
      }
    }

    int w = (int)((ASRankMediator)acom).getW();
    int r = 1, j = 0, l = 0;
    double dt = 0;
    for (int i : antmap.keySet()) {
      dt = (double)(w - (r++)) * (1.0/(double)antmap.get(i).getTourLength());
      for (int n = 0; n < acom.getNumOfCities(); n++) {
        j = antmap.get(i).getTour(n);
        l = antmap.get(i).getTour(n+1);
        acom.setPheromone(j, l, (acom.getPheromone(j, l) + dt) );
      }
    }
  }

  protected void enforceGlobalBest() {
    /* enforcement of global best route */
    int j = 0, l = 0;
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      j = acom.getGlobalBestTour(i);
      l = acom.getGlobalBestTour(i+1);
      acom.setPheromone( j, l, (acom.getPheromone(j,l) +
            ((ASRankMediator)acom).getW()/(double)acom.getGlobalBestTourLength()) );
    }
  }

  protected void depositPheromone(Ant ant) {
    double dt = 1.0/(double)ant.getTourLength();
    int j = 0;
    int l = 0;
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      j = ant.getTour(i);
      l = ant.getTour(i+1);
      /* setPheromone will update j,l and l,j */
      acom.setPheromone( j, l, (acom.getPheromone(j,l) + dt) );
    }
  }

}
