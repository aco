package aco.strategy.as.asrank;

import aco.strategy.*;
import aco.mediator.as.*;
import aco.mediator.as.asrank.*;

public class ASRankTauZeroStrategy
  extends TauZeroStrategy {

  public ASRankTauZeroStrategy(ASRankMediator acom) {
    super((ASMediator) acom);
  }

  public void computeTauZero() {
    acom.setTauZero(
        (((ASRankMediator)acom).getW()/2.0) *
        ( (((ASRankMediator)acom).getW() - 1) /
      (acom.getRoh() * (double)acom.nearestNeighbourHeuristicRandomStart() )));
  }

}
