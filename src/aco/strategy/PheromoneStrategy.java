package aco.strategy;

import aco.ant.*;
import aco.mediator.*;

public abstract class PheromoneStrategy {

  protected ACOMediator acom;

  public PheromoneStrategy(ACOMediator acom) {
    this.acom = acom;
  }

  public void setInitialPheromones(double TauZero) {
    for (int i = 0; i < acom.getNumOfCities(); i++) {
      for (int j = 0; j < acom.getNumOfCities(); j++) {
        acom.setPheromone(i,j, TauZero );
      }
    }
  }

  public abstract void pheromoneUpdate(Ant[] ants);

}
