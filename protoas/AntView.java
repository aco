package protoas;

import java.util.Observable;
import java.util.Observer;
import java.util.HashMap;
import java.awt.*;
import java.applet.*;
import javax.swing.*;

class AntView
  extends Applet
  implements Observer, Runnable {

  final static long serialVersionUID = 1L;

  protected int xMax;
  protected int yMax;
  protected int wXSize;
  protected int wYSize;
  protected double xScale;
  protected double yScale;
  protected double xMax2yMaxRatio;
  protected int[] xPoints;
  protected int[] yPoints;
  protected final int cityPointRadius = 10;
  protected final int borderSize = 20;

  protected JFrame jframe;
  protected Environment env;
  protected Graph graph;
  protected Coordinate coordinate;

  public AntView(Environment env) {
    this((int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.9),
        (int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.9), env);
  }

  public AntView(
      int wsize,
      Environment env) {
    this(wsize, wsize, env);
  }

  public AntView(
      int wxsize,
      int wysize,
      Environment env) {

    this.env = env;
    this.graph = env.getGraph();
    this.coordinate = graph.getCoordinate();

    this.xPoints = new int[graph.getNumOfCities() + 1];
    this.yPoints = new int[graph.getNumOfCities() + 1];

    this.xMax = 0;
    this.yMax = 0;

    for (int c = 0; c < graph.getNumOfCities(); c++) {
      xPoints[c] = coordinate.getCoordinates(c).getFirst();
      yPoints[c] = coordinate.getCoordinates(c).getSecond();
      if (xPoints[c] > xMax)
        xMax = xPoints[c];
      if (yPoints[c] > yMax)
        yMax = yPoints[c];
    }

    this.xMax2yMaxRatio = (double)xMax/(double)yMax;

    wXSize = (int)((double)xMax/(double)yMax * (double)wxsize);
    wYSize = wysize;

    createJFrame("Custom...", this, wXSize, wYSize);

    System.gc();
  }

  protected void createJFrame(String Title, Component windowComponent, int wxsize, int wysize) {
    jframe = new JFrame(Title);

    jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Container cp = jframe.getContentPane();
    cp.setBackground(Color.white);
    cp.add(BorderLayout.CENTER, windowComponent);

    jframe.pack();
    jframe.setSize(new Dimension(wxsize, wysize));
    jframe.setVisible(true);
    jframe.toFront();
  }

  public void run() {
    this.update(this.getGraphics());
  }

  public void paint(Graphics g) {
    Graphics2D g2 = (Graphics2D)g;
    HashMap<RenderingHints.Key, Object> rhmap = new HashMap<RenderingHints.Key, Object>(8);
    rhmap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_DEFAULT);
    rhmap.put(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    rhmap.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    rhmap.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
    rhmap.put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    rhmap.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    rhmap.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    rhmap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    RenderingHints rh = new RenderingHints(rhmap);
    g2.setRenderingHints(rh);

    this.update(g);
  }

  public void update(Graphics g) {

    Graphics2D g2 = (Graphics2D)g;
    Dimension d = this.getSize();

    if (d.getWidth() > (d.getHeight() * xMax2yMaxRatio)) {
      xScale = ((d.getHeight() - 2 * borderSize) * xMax2yMaxRatio) / (double)xMax;
      yScale = (d.getHeight() - 2 * borderSize) / (double)yMax;
    } else {
      xScale = (d.getWidth() - 2 * borderSize) / (double)xMax;
      yScale = ((d.getWidth() - 2 * borderSize) * 1/xMax2yMaxRatio) / (double)yMax;
    }

    for (int c = 0; c < graph.getNumOfCities(); c++) {
      xPoints[c] =
        (int)((double)coordinate.getCoordinates(
              env.getMinTour(c) ).getFirst() * xScale) + borderSize;
      yPoints[c] =
        d.height - (int)((double)coordinate.getCoordinates(
              env.getMinTour(c) ).getSecond() * yScale) - borderSize;
    }

    xPoints[graph.getNumOfCities()] = xPoints[0];
    yPoints[graph.getNumOfCities()] = yPoints[0];

    g2.clearRect(0, 0, d.width, d.height);

    drawTourLines(g2);
    drawCities(g2);

    g2.setColor(new Color(0, 0, 0));
    g2.drawString("Minimum Length: " +
        env.getMinTourLength() +
        " at Iteration " +
        env.getMinTourIteration(),
        10, d.height - 10);

    System.gc();
  }

  public void drawCities(Graphics2D g2) {
    Dimension d = this.getSize();

    int cpr = (int)cityPointRadius;
    if (d.getWidth() > d.getHeight())
      cpr = (int)Math.round(d.getHeight()/wYSize * cityPointRadius);
    else
      cpr = (int)Math.round(d.getWidth()/wXSize * cityPointRadius);

    if (cpr < 2)
      cpr = 2;

    int halfcpr = cpr >> 1;
    for (int c = 0; c < graph.getNumOfCities(); c++) {
      g2.setColor(new Color(0, 0, 128));
      g2.fillOval(xPoints[c] - halfcpr,
          yPoints[c] - halfcpr,
          cpr,
          cpr);
      g2.setColor(new Color(0, 128, 0));
      g2.drawString(Integer.toString(c),
          xPoints[c] + halfcpr,
          yPoints[c] - halfcpr);
    }

  }

  public void drawCityWeb(Graphics2D g2) {
    g2.setColor(new Color(210, 210, 210));
    g2.setStroke(new BasicStroke(0.25f));

    for (int c1 = 0; c1 < graph.getNumOfCities(); c1++) {
      for (int c2 = 0; c2 < graph.getNumOfCities(); c2++) {
        g2.drawLine(xPoints[c1], yPoints[c1], xPoints[c2], yPoints[c2]);
      }
    }
  }

  public void drawTourLines(Graphics2D g2) {
    g2.setColor(new Color(200, 20, 20));
    g2.setStroke(new BasicStroke(1.75f));
    g2.drawPolyline(xPoints, yPoints, graph.getNumOfCities() + 1);
  }

  public void update(Observable o, Object arg) {
    this.update(this.getGraphics());
  }

}
