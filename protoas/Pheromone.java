package protoas;

class Pheromone {

  protected Graph graph;
  protected double[][] Pheromone;

  Pheromone(Graph graph) {
    this.graph = graph;
    this.Pheromone = new double[graph.getNumOfCities()][graph.getNumOfCities()];

    double tauzero =
      (double)(graph.getNumOfCities()) /
      graph.nearestNeighbourHeuristicRandomStart();

    for (int i = 0; i < graph.getNumOfCities(); i++) {
      for (int j = 0; j < graph.getNumOfCities(); j++) {
        setPheromone(i,j, tauzero);
      }
    }

  }

  public void pheromoneUpdate(Ant[] ants) {
    evaporate();

    for (Ant ant : ants) {
      depositPheromone(ant);
    }

    graph.computeChoiceInformation();
  }

  public void evaporate() {
    for (int i = 1; i < graph.getNumOfCities(); i++) {
      for (int j = 1; j < graph.getNumOfCities(); j++) {
        Pheromone[i][j] = (1.0 - graph.getROH()) * Pheromone[i][j];
        Pheromone[j][i] = Pheromone[i][j];
      }
    }
  }

  public void depositPheromone(Ant ant) {
    double dt = 1.0/ant.getTourLength();
    int j = 0;
    int l = 0;
    for (int i = 0; i < graph.getNumOfCities() - 1; i++) {
      j = ant.getTour(i);
      l = ant.getTour(i+1);
      Pheromone[j][l] = Pheromone[j][l] + dt;
      Pheromone[l][j] = Pheromone[j][l];
    }
  }

  public double[][] getPheromone() {
    return this.Pheromone;
  }

  public double getPheromone(int x, int y) {
    return this.Pheromone[x][y];
  }

  public void setPheromone(double[][] Pheromone) {
    this.Pheromone = Pheromone;
  }

  public void setPheromone(int x, int y, double Pheromone) {
    this.Pheromone[x][y] = Pheromone;
  }

}
