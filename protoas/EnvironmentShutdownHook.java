package protoas;

class EnvironmentShutdownHook extends Thread {

  protected Environment env;

  EnvironmentShutdownHook(Environment env) {
    this.env = env;
  }

  public void run() {
    System.out.println(env);
  }
}
