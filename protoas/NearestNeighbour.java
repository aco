package protoas;

class NearestNeighbour {

  protected Graph graph;
  protected int[][] NearestNeighbour;

  NearestNeighbour(Graph graph) {
    this.graph = graph;
    this.NearestNeighbour =
      new int[graph.getNumOfCities()][graph.getNearestNeighboursDepth()];

    for (int i = 0; i < graph.getNumOfCities(); i++) {
      for (int j = 0; j < graph.getNearestNeighboursDepth(); j++) {
        setNearestNeighbour(i, j, -1);
      }
    }
  }

  public void computeNearestNeighbours() {
    for (int City = 0; City < graph.getNumOfCities(); City++) {
      for (int Neighbour = 0; Neighbour < graph.getNumOfCities(); Neighbour++) {

        if (City == Neighbour) {
          continue;
        } else {

          for (int d = 0; d < graph.getNearestNeighboursDepth(); d++) {

            /* if there is no entry yet, then assign */
            if (getNearestNeighbour(City,d) == -1) {
              setNearestNeighbour(City,d, Neighbour);
              break;
            }
            /* if distance from city to neighbour is smaller
             * than distance from city to neighbour from list */
            if (graph.getDistance(City,Neighbour) <
                graph.getDistance(City, getNearestNeighbour(City,d))) {

              /* copy element n-1 to n; right shift so to say; until elem d is reached */
              for (int c = graph.getNearestNeighboursDepth() - 1; c > d; c--) {
                setNearestNeighbour(City,c, getNearestNeighbour(City,c-1));
              }
              setNearestNeighbour(City,d, Neighbour);
              break;

                }

          }
        }
      }
    }
  }

  public int[][] getNearestNeighbour() {
    return this.NearestNeighbour;
  }

  public int getNearestNeighbour(int x, int y) {
    return this.NearestNeighbour[x][y];
  }

  public void setNearestNeighbour(int[][] NearestNeighbour) {
    this.NearestNeighbour = NearestNeighbour;
  }

  public void setNearestNeighbour(int x, int y, int NearestNeighbour) {
    this.NearestNeighbour[x][y] = NearestNeighbour;
  }

}
