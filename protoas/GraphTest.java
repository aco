package protoas;

import java.util.Vector;
import java.util.Iterator;
import si.ant.graph.*;

public class GraphTest {

  public static void MatrixGraphTest() {
    int n = 5;
    int[][] graph = new int[n][n];
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        graph[i][j] = MatrixGraph.INFINITY;
      }
    }

    graph[0][1] = 1;
    graph[1][2] = 2;
    graph[2][3] = 3;
    graph[3][4] = 4;

    MatrixGraph mg = new MatrixGraph(5, graph, graph, graph);

    Vector<MatrixNode> nodes = mg.getNodes();
    Vector<MatrixEdge> edges = mg.getEdges();

    Iterator<MatrixNode> nodesit = nodes.iterator();
    Iterator<MatrixEdge> edgesit = edges.iterator();

    System.out.println("Nodes: ");
    while (nodesit.hasNext())
      System.out.println(nodesit.next().toString());

    System.out.println("Edges: ");
    while (edgesit.hasNext())
      System.out.println(edgesit.next().toString());

    System.out.println("nodes.get(2).getIndex(): " + nodes.get(2).getIndex());
    System.out.println("true: " + nodes.get(2).isEqual(nodes.get(2)));
    System.out.println("false: " + nodes.get(2).isEqual(nodes.get(3)));
    System.out.println("3.0: " + edges.get(2).getDistance());
    System.out.println("true: " + edges.get(2).isEqual(edges.get(2)));
    System.out.println("false: " + edges.get(2).isEqual(edges.get(3)));

    System.out.println("neighbours of " + nodes.get(3) + ":");
    Vector<MatrixNode> neighbours = mg.getNeighbours(nodes.get(3));
    Iterator<MatrixNode> neighboursit = neighbours.iterator();

    while (neighboursit.hasNext())
      System.out.println(neighboursit.next().toString());
  }

  public static void ListGraphTest() {
    Vector<ListEdge> edgegraph = new Vector<ListEdge>(4);
    ListNode node0 = new ListNode("nil");
    ListNode node1 = new ListNode("one");
    ListNode node2 = new ListNode("two");
    ListNode node3 = new ListNode("three");
    ListNode node4 = new ListNode("four");
    edgegraph.add(new ListEdge(node0, node1, 0, 1, 0));
    edgegraph.add(new ListEdge(node1, node2, 0, 2, 0));
    edgegraph.add(new ListEdge(node2, node3, 0, 3, 0));
    edgegraph.add(new ListEdge(node3, node4, 0, 4, 0));

    ListGraph lg = new ListGraph(edgegraph);

    Vector<ListNode> nodes = lg.getNodes();
    Vector<ListEdge> edges = lg.getEdges();

    Iterator<ListNode> nodesit = nodes.iterator();
    Iterator<ListEdge> edgesit = edges.iterator();

    System.out.println("Nodes: ");
    while (nodesit.hasNext())
      System.out.println(nodesit.next().toString());

    System.out.println("Edges: ");
    while (edgesit.hasNext())
      System.out.println(edgesit.next().toString());

    System.out.println("nodes.get(2).getID(): " + nodes.get(2).getID());
    System.out.println("true: " + nodes.get(2).isEqual(nodes.get(2)));
    System.out.println("false: " + nodes.get(2).isEqual(nodes.get(3)));
    System.out.println("3.0: " + edges.get(2).getDistance());
    System.out.println("true: " + edges.get(2).isEqual(edges.get(2)));
    System.out.println("false: " + edges.get(2).isEqual(edges.get(3)));

    System.out.println("neighbours of " + nodes.get(3) + ":");
    Vector<ListNode> neighbours = lg.getNeighbours(nodes.get(3));
    Iterator<ListNode> neighboursit = neighbours.iterator();

    while (neighboursit.hasNext())
      System.out.println(neighboursit.next().toString());
  }

  public static void main(String[] args) {
    GraphTest.MatrixGraphTest();
    System.out.println("==============");
    GraphTest.ListGraphTest();
  }

}
