package protoas;

import java.io.IOException;

class Graph {

  public final int INFINITY = Integer.MAX_VALUE;
  public final double ALPHA = 1.0;
  public final double BETA = 5.0;
  public final double ROH = 0.2;

  protected int NumOfCities;
  protected int NearestNeighboursDepth;

  public String[] ids = null;

  // NxN; Distance Matrix
  protected Distance distance;
  // NxN; Pheromone Matrix
  protected Pheromone pheromone;
  // NxNN; Nearest Neighbours List of depth NN
  protected NearestNeighbour nearestneighbour;
  // NxN; combined pheromone and heuristic information
  protected ChoiceInformation choiceinformation;

  protected Coordinate coordinate = null;

  Graph(String filename) throws IOException {
    TSPLibReader tsplibreader = new TSPLibReader(filename);
    this.coordinate = new Coordinate(this, tsplibreader);

    if (NumOfCities <= 30)
      this.NearestNeighboursDepth = NumOfCities - 1;
    else if (NumOfCities > 30 && NumOfCities < 80)
      this.NearestNeighboursDepth = NumOfCities/2;
    else
      this.NearestNeighboursDepth = 40;

    this.distance = new Distance(this, coordinate.computeDistances());
    this.pheromone = new Pheromone(this);
    this.nearestneighbour = new NearestNeighbour(this);
    this.choiceinformation = new ChoiceInformation(this);

    computeNearestNeighbours();
    computeChoiceInformation();
  }

  Graph(int NumOfCities) {
    if (NumOfCities <= 30)
      this.NearestNeighboursDepth = NumOfCities - 1;
    else if (NumOfCities > 30 && NumOfCities < 80)
      this.NearestNeighboursDepth = NumOfCities/2;
    else
      this.NearestNeighboursDepth = 40;

    this.NumOfCities = NumOfCities;

    this.distance = new Distance(this);
    this.pheromone = new Pheromone(this);
    this.nearestneighbour = new NearestNeighbour(this);
    this.choiceinformation = new ChoiceInformation(this);
  }

  Graph(int NumOfCities, int NearestNeighboursDepth) {
    this.NumOfCities = NumOfCities;
    this.NearestNeighboursDepth = NearestNeighboursDepth;

    this.distance = new Distance(this);
    this.pheromone = new Pheromone(this);
    this.nearestneighbour = new NearestNeighbour(this);
    this.choiceinformation = new ChoiceInformation(this);
  }

  public void pheromoneUpdate(Ant[] ants) {
    pheromone.pheromoneUpdate(ants);
  }

  public void computeNearestNeighbours() {
    this.nearestneighbour.computeNearestNeighbours();
  }

  public void computeChoiceInformation() {
    this.choiceinformation.computeChoiceInformation();
  }

  public int nearestNeighbourHeuristic(int city, boolean[] visited, int remaining) {

    int nextmin = INFINITY;
    int nextcity = 0;

    for (int i = 0; i < getNumOfCities(); i++) {
        if ((i != city) && (!visited[i]) && (getDistance(i,city) < nextmin)) {
          nextmin = getDistance(i,city);
          nextcity = i;
        }
    }

    visited[nextcity] = true;

    if (remaining == 1)
      return nextmin;
    else
      return nextmin + nearestNeighbourHeuristic(nextcity, visited, remaining - 1);
  }

  public int nearestNeighbourHeuristicRandomStart() {
    boolean[] visited = new boolean[getNumOfCities()];
    for (int i = 0; i < getNumOfCities(); i++)
      visited[i] = false;
    return nearestNeighbourHeuristic(
        (int)(Math.random() * getNumOfCities()),
        visited, getNumOfCities());
  }

  public Boolean hasCoordinates() {
    return coordinate != null;
  }

  public Boolean hasIds() {
    return ids != null;
  }

  public int getNumOfCities() {
    return this.NumOfCities;
  }

  public int getNearestNeighboursDepth() {
    return this.NearestNeighboursDepth;
  }

  public int getDistance(int x, int y) {
    return this.distance.getDistance(x, y);
  }

  public Coordinate getCoordinate() {
    return this.coordinate;
  }

  public double getPheromone(int x, int y) {
    return this.pheromone.getPheromone(x, y);
  }

  public int getNearestNeighbour(int x, int y) {
    return this.nearestneighbour.getNearestNeighbour(x, y);
  }

  public double getChoiceInformation(int x, int y) {
    return this.choiceinformation.getChoiceInformation(x, y);
  }

  public double getALPHA() {
    return this.ALPHA;
  }

  public double getBETA() {
    return this.BETA;
  }

  public double getROH() {
    return this.ROH;
  }

  public int getINFINITY() {
    return this.INFINITY;
  }

  public void setNumOfCities(int NumOfCities) {
    this.NumOfCities = NumOfCities;
  }

  public void setNearestNeighboursDepth(int NearestNeighboursDepth) {
    this.NearestNeighboursDepth = NearestNeighboursDepth;
  }

  public String[] getIds() {
    return this.ids;
  }

  public String getIds(int index) {
    if (this.ids != null) {
      return this.ids[index];
    } else {
      return null;
    }
  }

  public void setIds(String[] ids) {
    this.ids = ids;
  }

  public void setIds(int index, String ids) {
    this.ids[index] = ids;
  }

  protected void setDistance(int x, int y, int Distance) {
    this.distance.setDistance(x,y, Distance);
  }

  protected void setDistance(int[][] Distance) {
    this.distance.setDistance(Distance);
  }

  protected void mirrorDistances() {
    for (int i = 0; i < NumOfCities; i++) {
      for (int j = 0; j < NumOfCities; j++) {
        if (distance.getDistance(i,j) != getINFINITY()) {
          distance.setDistance(j,i, distance.getDistance(i,j));
        } else {
          distance.setDistance(i,j, distance.getDistance(j,i));
        }
      }
    }
  }

  protected void printNearestNeighbours() {
    for (int c = 0; c < getNumOfCities(); c++) {
      if (getIds(c) != null) {
        System.out.println("Nearest neighbours of: " + getIds(c));
      } else {
        System.out.println("Nearest neighbours of: " + c);
      }
      for (int n = 0; n < getNearestNeighboursDepth(); n++) {
        if (nearestneighbour.getNearestNeighbour(c,n) != -1) {
          if ( getIds( nearestneighbour.getNearestNeighbour(c,n) ) != null ) {
            System.out.print(
              getIds( nearestneighbour.getNearestNeighbour(c,n) ) + "\t");
          } else {
            System.out.print(nearestneighbour.getNearestNeighbour(c,n) + "\t");
          }
        } else {
          System.out.print("none" + "\t");
        }
      }
      System.out.println();
    }
  }

  @Override
    public String toString() {
      StringBuilder result = new StringBuilder();

      if (getIds() != null) {
        for (String id : getIds())
          result.append("\t" + id);
      } else {
        for (int i = 0; i < NumOfCities; i++)
          result.append("\t" + i);
      }

      int l = 0;
      for (int i[] : distance.getDistance()) {
        if (ids != null) {
          if (ids[l] != null) {
            result.append("\n" + ids[l++] + "\t");
          } else {
            result.append("\n" + (l++) + "\t");
          }
        } else {
            result.append("\n" + (l++) + "\t");
        }

        for (int j : i) {
          if (j == INFINITY) {
            result.append("INF\t");
          } else {
            result.append(j + "\t");
          }
        }
      }

      return result.toString();
    }

  public static Graph sampleGraph() {

    Graph graph = new Graph(11, 10);

    int[][] Distances = new int[11][11];

    /* Nuernberg - Leipzig */
    Distances[0][1] = 269;
    /* Nuernberg - Dresden */
    Distances[0][2] = 313;
    /* Nuernberg - Berlin */
    Distances[0][3] = 441;
    /* Nuernberg - Hamburg */
    Distances[0][4] = 609;
    /* Nuernberg - Bremen */
    Distances[0][5] = 582;
    /* Nuernberg - Hannover */
    Distances[0][6] = 465;
    /* Nuernberg - Koeln */
    Distances[0][7] = 410;
    /* Nuernberg - Frankfurt */
    Distances[0][8] = 225;
    /* Nuernberg - Stuttgart */
    Distances[0][9] = 208;
    /* Nuernberg - Muenchen */
    Distances[0][10] = 166;

    /* Leipzig - Dresden */
    Distances[1][2] = 116;
    /* Leipzig - Berlin */
    Distances[1][3] = 195;
    /* Leipzig - Hamburg */
    Distances[1][4] = 396;
    /* Leipzig - Bremen */
    Distances[1][5] = 369;
    /* Leipzig - Hannover */
    Distances[1][6] = 264;
    /* Leipzig - Koeln */
    Distances[1][7] = 498;
    /* Leipzig - Frankfurt */
    Distances[1][8] = 391;
    /* Leipzig - Stuttgart */
    Distances[1][9] = 478;
    /* Leipzig - Muenchen */
    Distances[1][10] = 430;

    /* Dresden - Berlin */
    Distances[2][3] = 194;
    /* Dresden - Hamburg */
    Distances[2][4] = 477;
    /* Dresden - Bremen */
    Distances[2][5] = 473;
    /* Dresden - Hannover */
    Distances[2][6] = 367;
    /* Dresden - Koeln */
    Distances[2][7] = 573;
    /* Dresden - Frankfurt */
    Distances[2][8] = 463;
    /* Dresden - Stuttgart */
    Distances[2][9] = 510;
    /* Dresden - Muenchen */
    Distances[2][10] = 465;

    /* Berlin - Hamburg */
    Distances[3][4] = 288;
    /* Berlin - Bremen */
    Distances[3][5] = 392;
    /* Berlin - Hannover */
    Distances[3][6] = 286;
    /* Berlin - Koeln */
    Distances[3][7] = 575;
    /* Berlin - Frankfurt */
    Distances[3][8] = 547;
    /* Berlin - Stuttgart */
    Distances[3][9] = 633;
    /* Berlin - Muenchen */
    Distances[3][10] = 585;

    /* Hamburg - Bremen */
    Distances[4][5] = 122;
    /* Hamburg - Hannover */
    Distances[4][6] = 157;
    /* Hamburg - Koeln */
    Distances[4][7] = 426;
    /* Hamburg - Frankfurt */
    Distances[4][8] = 493;
    /* Hamburg - Stuttgart */
    Distances[4][9] = 655;
    /* Hamburg - Muenchen */
    Distances[4][10] = 775;

    /* Bremen - Hannover */
    Distances[5][6] = 131;
    /* Bremen - Koeln */
    Distances[5][7] = 315;
    /* Bremen - Frankfurt */
    Distances[5][8] = 441;
    /* Bremen - Stuttgart */
    Distances[5][9] = 629;
    /* Bremen - Muenchen */
    Distances[5][10] = 749;

    /* Hannover - Koeln */
    Distances[6][7] = 295;
    /* Hannover - Frankfurt */
    Distances[6][8] = 349;
    /* Hannover - Stuttgart */
    Distances[6][9] = 512;
    /* Hannover - Muenchen */
    Distances[6][10] = 632;

    /* Koeln - Frankfurt */
    Distances[7][8] = 194;
    /* Koeln - Stuttgart */
    Distances[7][9] = 369;
    /* Koeln - Muenchen */
    Distances[7][10] = 576;

    /* Frankfurt - Stuttgart */
    Distances[8][9] = 209;
    /* Frankfurt - Muenchen */
    Distances[8][10] = 393;

    /* Stuttgart - Muenchen */
    Distances[9][10] = 220;

    graph.setDistance(Distances);

    String[] ids = {"NBG", "Leipzip", "Dresden", "Berlin", "Hamburg",
                    "Bremen", "HAN", "Koeln", "FFM", "STR",
                    "MUC"};
    graph.ids = ids;

    graph.mirrorDistances();
    graph.computeNearestNeighbours();
    graph.computeChoiceInformation();

    return graph;
  }

  public static void main (String[] args) {
    try {
      Graph graph;
      if (args.length == 1)
        graph = new Graph(args[0]);
      else
        graph = Graph.sampleGraph();
      System.out.println(graph);
      graph.printNearestNeighbours();
    } catch (Exception e) {
      System.out.println(e.toString() + "(" + e.getClass() + "): " + e);
      System.exit(1);
    }
  }

}
