package protoas;

import java.util.HashMap;

class Coordinate {

  protected Graph graph;
  protected final HashMap<Integer, CoordinatePair<Integer, Integer>> Coordinates;

  Coordinate(Graph graph) {
    this.graph = graph;
    this.Coordinates = new HashMap<Integer, CoordinatePair<Integer, Integer>>(graph.getNumOfCities());
  }

  Coordinate(Graph graph, TSPLibReader tsplibreader) {
    this.graph = graph;
    this.Coordinates = tsplibreader.getCoordinates();
    graph.setNumOfCities(Coordinates.size());
  }

  public int[][] computeDistances() {

    int[][] Distance = new int[graph.getNumOfCities()][graph.getNumOfCities()];

    for (int k1 : Coordinates.keySet()) {
      for (int k2 : Coordinates.keySet()) {
        if (k1 == k2)
          Distance[k1][k2] = graph.getINFINITY();
        else
          Distance[k1][k2] = computeDistance(getCoordinates(k1), getCoordinates(k2));
      }
    }

    return Distance;
  }

  public int computeDistance(CoordinatePair<Integer, Integer> CityOne,
      CoordinatePair<Integer, Integer> CityTwo) {
    /* pythagoras: a^2 + b^2 = c^2 => c = sqrt( a^2 + b^2 ) */
    return (int)Math.sqrt(
        Math.pow((double)(CityOne.getFirst() - CityTwo.getFirst()), 2) +
        Math.pow( (double)(CityOne.getSecond() - CityTwo.getSecond()), 2 ) );

    /* ATT */
    /*
    double xd = (double)(CityOne.getFirst() - CityTwo.getFirst());
    double yd = (double)(CityOne.getSecond() - CityTwo.getSecond());
    double rij = Math.sqrt( (xd*xd + yd*yd) / 10.0 );
    return (int)Math.round(rij);
    */
  }

  public CoordinatePair<Integer, Integer> getCoordinates(int City) {
    return Coordinates.get(City);
  }

}
