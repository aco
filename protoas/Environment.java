package protoas;

import java.lang.Runtime;
import java.util.Observable;

class Environment extends Observable {

  public final int NumOfAnts;
  public final int MaxNumOfTours;

  protected int[] MinTour;
  protected int MinTourLength;
  protected int MinTourIteration;

  protected Ant[] ants;
  protected Graph graph;

  protected AntView av;

  Environment(int MaxNumOfTours, Graph graph) {
    this(MaxNumOfTours, 0, graph.NumOfCities, graph);
  }

  Environment(int MaxNumOfTours, int GuiSize, Graph graph) {
    this(MaxNumOfTours, GuiSize, graph.NumOfCities, graph);
  }

  Environment(int MaxNumOfTours, int GuiSize, int NumOfAnts, Graph graph) {

    this.MaxNumOfTours = MaxNumOfTours;

    this.MinTour = new int[graph.getNumOfCities()];
    this.MinTourLength = graph.getINFINITY();
    this.MinTourIteration = 0;

    this.NumOfAnts = NumOfAnts;
    this.ants = new Ant[NumOfAnts];
    this.graph = graph;

    for (int i = 0; i < NumOfAnts; i++)
      ants[i] = new Ant(graph);

    if (graph.hasCoordinates()) {
      if (GuiSize == 0)
        av = new AntView(this);
      else
        av = new AntView(GuiSize, this);
      this.addObserver(av);
      new Thread(av).start();
    }

    /* add a shutdown hook to print out the last best solution */
    Runtime.getRuntime().addShutdownHook(new EnvironmentShutdownHook(this));

    /* run the garbage collector after all the initialization is done */
    System.gc();
  }

  /* d'tor */
  protected void finalize() {
    deleteObservers();
  }

  public void run() {
    int i = 0;
    while (! terminateCondition(i++)) {
      constructSolutions();
      graph.pheromoneUpdate(ants);
      updateStatistics(i);
      /* localSearch();
       * updatePheromoneTrails(); */
    }
  }

  protected void constructSolutions() {

    /* clear ants memory and ..
     * assign an initial random city to ant */
    /* TODO: threading */
    for (Ant ant : ants) {
      ant.clearData();
      ant.pickInitialRandomCity();
    }

    /* let each ant construct a complete tour 
     * start a 1 since every ant already has an initial city */
    /* TODO: threading */
    for (int step = 1; step < graph.getNumOfCities(); step++) {
      for (Ant ant : ants)
        ant.neighbourListASDecisionRule(step);
    }

    /* compute tour length of each ant */
    /* TODO: threading */
    for (Ant ant : ants) {
      ant.computeTourLength();
    }


  }

  protected void updateStatistics(int iteration) {
    for (Ant ant : ants) {

      if (ant.getTourLength() < getMinTourLength()) {
        setMinTourLength(ant.getTourLength());
        setMinTourIteration(iteration);

        for (int i = 0; i < graph.NumOfCities; i++) {
          setMinTour(i, ant.getTour(i));
        }

        if (graph.hasCoordinates()) {
          this.setChanged();
          this.notifyObservers(this);
        }
      }

    }
    /* since we are already at it.. run the garbage collector */
    System.gc();
  }

  protected boolean stagnationBehaviour() {
    for (Ant antOne : ants) {
      for (Ant antTwo : ants) {
        for (int i : antOne.getTour()) {
          for (int j : antTwo.getTour()) {
            if (i != j)
              return false;
          }
        }
      }
    }
    return true;
  }

  protected boolean terminateCondition(int iteration) {
    /* 1) solution within predefined distance
     * 2) max number of tour constructions or algorithm iterations
     * 3) max cpu time
     * 4) algorithm stagnation behaviour */
    if (iteration >= MaxNumOfTours) {
      return true;
    //} else if (stagnationBehaviour()) {
      //return true;
    } else {
      return false;
    }
  }

  @Override
    public String toString() {
      StringBuilder result = new StringBuilder();
      result.append("Shortest tour at iteration " +
          MinTourIteration +
          " with length " +
          MinTourLength +
          "\n");

      for (int t : MinTour) {
        if (graph.getIds(t) != null)
          result.append(graph.ids[t] + "\t");
        else
          result.append(t + "\t");
      }
      if (graph.getIds(0) != null)
        result.append(graph.ids[MinTour[0]]);
      else
        result.append(MinTour[0]);
      return result.toString();
    }

  public int[] getMinTour() {
    return this.MinTour;
  }

  public int getMinTour(int index) {
    return this.MinTour[index];
  }

  public void setMinTour(int[] MinTour) {
    this.MinTour = MinTour;
  }

  public void setMinTour(int index, int MinTour) {
    this.MinTour[index] = MinTour;
  }

  public int getMinTourLength() {
    return this.MinTourLength;
  }

  public void setMinTourLength(int MinTourLength) {
    this.MinTourLength = MinTourLength;
  }

  public int getMinTourIteration() {
    return this.MinTourIteration;
  }

  public void setMinTourIteration(int MinTourIteration) {
    this.MinTourIteration = MinTourIteration;
  }

  public Graph getGraph() {
    return this.graph;
  }

  public void setGraph(Graph graph) {
    this.graph = graph;
  }

}
