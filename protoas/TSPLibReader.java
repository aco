package protoas;

import java.util.HashMap;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.StreamTokenizer;
import java.io.BufferedInputStream;

class TSPLibReader {

  final static long serialVersionUID = (long)1.0;

  protected StreamTokenizer streamtokenizer;
  protected HashMap<Integer, CoordinatePair<Integer, Integer>> coordinates;

  TSPLibReader(String filename) throws IOException {
    this(new File(filename));
  }

  TSPLibReader(File file) throws IOException {
    coordinates =
      new HashMap<Integer, CoordinatePair<Integer, Integer>>(countLines(new FileInputStream(file)));

    streamtokenizer = new StreamTokenizer(new BufferedReader(new FileReader(file)));
    streamtokenizer.eolIsSignificant(true);
    streamtokenizer.wordChars('_','_');
  }

  public HashMap<Integer, CoordinatePair<Integer, Integer>> getCoordinates() {
    try {
      this.parseLines();
    } catch (IOException e) {
      System.out.println(e.toString() + "(" + e.getClass() + "): " + e);
      return null;
    }
    return coordinates;
  }

  protected void parseLines() throws IOException {

    boolean SeenDisplayDataSection = false;
    int i = 0, x = 0, y = 0, City = 0;

    do {

      if (!SeenDisplayDataSection) {
        streamtokenizer.nextToken();
        //System.out.println(streamtokenizer);
        if (streamtokenizer.ttype == StreamTokenizer.TT_WORD)
          if (streamtokenizer.sval.equals("DISPLAY_DATA_SECTION") ||
              streamtokenizer.sval.equals("NODE_COORD_SECTION"))
            SeenDisplayDataSection = true;

        continue;
      }

      streamtokenizer.nextToken();
      if (streamtokenizer.ttype == StreamTokenizer.TT_NUMBER) {
        i++;
      } else {
        i = 0;
        continue;
      }

      streamtokenizer.nextToken();
      if (streamtokenizer.ttype == StreamTokenizer.TT_NUMBER) {
        x = (int)streamtokenizer.nval;
        i++;
      } else {
        i = 0;
        continue;
      }

      streamtokenizer.nextToken();
      if (streamtokenizer.ttype == StreamTokenizer.TT_NUMBER) {
        y = (int)streamtokenizer.nval;
        i++;
      } else {
        i = 0;
        continue;
      }

      streamtokenizer.nextToken();
      if (streamtokenizer.ttype == StreamTokenizer.TT_EOL && i == 3)
        coordinates.put(City++, new CoordinatePair<Integer, Integer>(x, y));

      i = 0;

    } while (streamtokenizer.ttype != StreamTokenizer.TT_EOF);

  }

  protected int countLines(FileInputStream fis) throws IOException {
    InputStream is = new BufferedInputStream(fis);
    byte[] c = new byte[1024];
    int count = 0;
    int readChars = 0;
    while ((readChars = is.read(c)) != -1) {
      for (int i = 0; i < readChars; ++i) {
        if (c[i] == '\n')
          ++count;
      }
    }
    return count;
  }

  public static void main(String[] args) {
    try {
      TSPLibReader tlr = new TSPLibReader(args[0]);

      HashMap<Integer, CoordinatePair<Integer, Integer>> coordinates =
        tlr.getCoordinates();

      System.out.println("Entries in HashMap: " + coordinates.size());

      for (int k : coordinates.keySet())
        System.out.println("City " + k + " with Coordinates " +
            coordinates.get(k).getFirst() +
            "/" +
            coordinates.get(k).getSecond());

    } catch (IOException e) {
      System.out.println(e.toString() + "(" + e.getClass() + "): " + e);
      System.exit(1);
    }
  }

}
