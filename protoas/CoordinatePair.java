package protoas;

class CoordinatePair<F, S> {
  private final F first;
  private final S second;

  CoordinatePair(F first, S second) {
    this.first = first;
    this.second = second;
  }

  public F getFirst() {
    return this.first;
  }

  public S getSecond() {
    return this.second;
  }

}
