package protoas;

class Distance {

  protected Graph graph;
  protected int[][] Distance;

  Distance(Graph graph) {
    this(graph, new int[graph.getNumOfCities()][graph.getNumOfCities()]);

    for (int i = 0; i < graph.getNumOfCities(); i++) {
      for (int j = 0; j < graph.getNumOfCities(); j++) {
        setDistance(i,j, graph.getINFINITY());
      }
    }

  }

  Distance(Graph graph, int[][] Distance) {
    this.graph = graph;
    this.Distance = Distance;
  }

  public int[][] getDistance() {
    return this.Distance;
  }

  public int getDistance(int x, int y) {
    return this.Distance[x][y];
  }

  public void setDistance(int[][] Distance) {
    this.Distance = Distance;
  }

  public void setDistance(int x, int y, int Distance) {
    this.Distance[x][y] = Distance;
  }

}
