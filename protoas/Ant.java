package protoas;

class Ant {

  public int TourLength; // the ant's tour length
  public int[] Tour; // N+1; ant's memory storing (partial) tours
  public boolean[] Visited; // N; visited cities

  public Graph graph;

  Ant(Graph graph) {

    this.graph = graph;

    TourLength = 0;

    Tour = new int[graph.getNumOfCities()];
    Visited = new boolean[graph.getNumOfCities()];

    clearTour();
    clearMemory();
  }

  public void computeTourLength() {
    for (int i = 0; i < graph.getNumOfCities() - 1; i++) {
      TourLength += graph.getDistance(Tour[i], Tour[i+1]);
    }
    TourLength +=
      graph.getDistance(Tour[graph.getNumOfCities() - 1], Tour[0]);
  }

  public void pickInitialRandomCity() {
    int r = (int)(Math.random() * graph.NumOfCities);
    Tour[0] = r;
    Visited[r] = true;
  }

  public void neighbourListASDecisionRule(int Step) {

    int CurrentCity = Tour[Step - 1];

    double SumProbabilities = 0.0;
    double SelectionProbability[] = new double[graph.getNearestNeighboursDepth()];

    for (int j = 0; j < graph.getNearestNeighboursDepth(); j++) {

      if ( Visited[ graph.getNearestNeighbour(CurrentCity,j) ] ) {
        SelectionProbability[j] = 0.0;
      } else {
        SelectionProbability[j] =
          graph.getChoiceInformation( CurrentCity,
              graph.getNearestNeighbour(CurrentCity,j) );
        SumProbabilities += SelectionProbability[j];
      }

    }

    if (SumProbabilities == 0.0) {
      chooseBestNext(Step);
    } else {

      int j = 0;
      double r = Math.random() * SumProbabilities;
      double p = SelectionProbability[j];

      while (p < r) {
        p += SelectionProbability[++j];
      }

      Tour[Step] = graph.getNearestNeighbour(CurrentCity,j);
      Visited[ graph.getNearestNeighbour(CurrentCity,j) ] = true;
    }

  }

  public void chooseBestNext(int Step) {
    int nc = 0;
    /* this seems to be neccessary because some choice information
     * tends to become zero after a while */
    double v = -1.0;
    int CurrentCity = Tour[Step - 1];

    for (int j = 0; j < graph.getNumOfCities(); j++) {
      if (! Visited[j]) {
        if (graph.getChoiceInformation(CurrentCity,j) > v) {
          nc = j;
          v = graph.getChoiceInformation(CurrentCity,j);
        }
      }
    }

    Tour[Step] = nc;
    Visited[nc] = true;
  }

  public int[] getTour() {
    return this.Tour;
  }

  public int getTour(int Tour) {
    return this.Tour[Tour];
  }

  public int getTourLength() {
    return this.TourLength;
  }

  public void setTourLength(int TourLength) {
    this.TourLength = TourLength;
  }

  public void clearData() {
    clearTour();
    clearTourLength();
    clearMemory();
  }

  protected void clearTourLength() {
    TourLength = 0;
  }

  protected void clearTour() {
    for (int i = 0; i < graph.NumOfCities; i++)
      Tour[i] = 0;
  }

  protected void clearMemory() {
    for (int i = 0; i < graph.NumOfCities; i++)
      Visited[i] = false;
  }

  @Override
    public String toString() {
      StringBuilder result = new StringBuilder();

      result.append("Tour with length: " + TourLength + "\n");

      for (int t : Tour) {
        if (graph.ids[t] != null)
          result.append(graph.ids[t] + "\t");
        else
          result.append(t + "\t");
      }
      if (graph.ids[0] != null)
          result.append(graph.ids[Tour[0]]);
        else
          result.append(Tour[0]);

      return result.toString();
    }

}
