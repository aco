package protoas;

class ChoiceInformation {

  protected Graph graph;
  protected double[][] ChoiceInformation;

  ChoiceInformation(Graph graph) {
    this.graph = graph;
    ChoiceInformation = new double[graph.getNumOfCities()][graph.getNumOfCities()];

    for (int i = 0; i < graph.getNumOfCities(); i++) {
      for (int j = 0; j < graph.getNumOfCities(); j++) {
        setChoiceInformation(i,j, 0);
      }
    }
  }

  public void computeChoiceInformation() {
    for (int i = 0; i < graph.getNumOfCities(); i++) {
      for (int j = 0; j < graph.getNumOfCities(); j++) {
        setChoiceInformation(i,j,
          Math.pow( graph.getPheromone(i,j), graph.getALPHA() ) *
          Math.pow( (1.0/(double)graph.getDistance(i,j)), graph.getBETA() ));
      }
    }
  }

  public double[][] getChoiceInformation() {
    return this.ChoiceInformation;
  }

  public double getChoiceInformation(int x, int y) {
    return this.ChoiceInformation[x][y];
  }

  public void setChoiceInformation(double[][] ChoiceInformation) {
    this.ChoiceInformation = ChoiceInformation;
  }

  public void setChoiceInformation(int x, int y, double ChoiceInformation) {
    this.ChoiceInformation[x][y] = ChoiceInformation;
  }
}
