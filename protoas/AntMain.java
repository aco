package protoas;

import java.io.IOException;

class AntMain {

  public static void main(String[] args) {
    try {

      Graph graph;
      Environment env;
      int iterations = 0, guisize = 0;

      try {
        iterations = Integer.parseInt(args[0]);
      } catch (Exception e) {
        System.exit(1);
      }

      if (args.length == 2) {
        try {
          guisize = Integer.parseInt(args[1]);
        } catch (Exception e) { }

        if (guisize == 0) {
          graph = new Graph(args[1]);
        } else {
          graph = Graph.sampleGraph();
        }
        env = new Environment(iterations, graph);

        env.run();
      } else if (args.length == 3) {

        guisize = Integer.parseInt(args[1]);
        graph = new Graph(args[2]);
        env = new Environment(iterations, guisize, graph);

        env.run();
      } else {
        System.exit(1);
      }

      System.out.print("Computation finished..");
      /* wait for a keypress before exit */
      System.in.read();

    } catch (IOException e) {
      System.out.println(e.toString() + "(" + e.getClass() + "): " + e);
    }

    System.exit(0);
  }

}
