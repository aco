package si.ant.tsp;

public interface AntStrategy {
	public void runAlgorithm(int runs);
	public void view();
}
