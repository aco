package si.ant.tsp;

public class TSPAnt {
	public int tour_length;
	public int[] tour;
	public boolean[] visited;
	
	public TSPAnt(int n){
		tour = new int[n+1];
		visited = new boolean[n];
	}

}
