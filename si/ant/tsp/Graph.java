package si.ant.tsp;
import java.util.Vector;

public class Graph {
	public int[][] distance_matrix;
	public int[][] nn_list;//last element for each neighbor list is current city self
	public double[][] pheromones;
	public double[][] choice_info;
	public Vector<Node> nodes;
	public Graph(int n){
		nodes = new Vector<Node>(n);
		distance_matrix = new int[n][n];
		nn_list = new int[n][n];
		pheromones = new double[n][n];
		choice_info = new double[n][n];
	}
}
