package si.ant.tsp;
import java.util.Random;
import java.lang.Math;
import java.util.Arrays;

public class ACSAntAlgorithm implements AntStrategy{
	private double xi;
	public int bs_tour_length;
	public int[] bs_tour;
	public double tauZero;
	public double beta;
	public double qZero;
	public double rho;
	public Random random;
	public int cl;
	public TSPAnt[] ant;
	public int runs;
	public int n;//number of cities
	public int m;//number of ants
	public int[] x;
	public int[] y;
	public Graph g;
	
	
	public ACSAntAlgorithm(Graph g, int numbOfAnts, int n, int[] x, int[] y, //replace x, y with name of tsp-file
			double tauZero, double beta, double qZero, double rho, int cl, double xi){
		this.qZero = qZero;
		this.tauZero = tauZero;
		this.rho = rho;
		this.g = g;
		this.m = numbOfAnts;
		this.n = n;
		this.x = x;
		this.y = y;
		this.beta = beta;
		this.xi = xi;
		this.cl = cl;
		bs_tour = new int[n+1];
		this.random = new Random(System.nanoTime());
		for(int i=0; i<n; i++){
			for(int j=0; j<n; j++){
				//initialize pheromones
				g.pheromones[i][j] = tauZero;
				g.pheromones[j][i] = tauZero;
			}
		}
	
		
		
		//initializeData();
	}
	//called by Environment
	public void runAlgorithm(int runs){
		this.runs = runs;
		initializeData();
		for(int i=0; i<runs; i++){
			constructSolutions();
			updateStatistics();
			updatePheromoneTrails();//global pheromone update
			innerView(i+1);
		}
	}
	public void constructSolutions(){
		for(int k=0; k<m; k++){  //for every ant
			for(int i=0; i<n; i++){
				ant[k].visited[i] = false;
			}
		}
		int step = 0;
		int r=0;
		for(int k = 0; k<m; k++){
			r= random.nextInt(n); //random between 0..n-1
			ant[k].tour[step] = r;//set start city in tour[0]
			ant[k].visited[r] = true;
		}
		while(step < n){//for each city
			step = step + 1;
			for(int k=0; k<m; k++){
				ACSDecisionRule(k, step);
				ACSLocalPheromoneUpdate(k, step);
				random = new Random(random.nextLong());
				//view();
			}
		}
		for(int k=0; k<m; k++){
			ant[k].tour[n] = ant[k].tour[0]; //n+1
			ant[k].tour_length = computeTourLength(k); //for every ant k
		}
		random = new Random(r);
	}
	public void initializeData(){
		readInstance();
		computeDistances();
		computeNearestNeighborLists();
		computeChoiceInformation();
		initializeAnts();
		//initializeParameters();
		initializeStatistics();
	}
	public void readInstance(){
		//initialize every Node with x,y und s   //open tsp-file and set x and y - arrays
		for(int i= 0; i<x.length ; i++){
			String s = " " + i;
			Node n = new Node(s, x[i],y[i]);
			g.nodes.add(n);
		}
	}
	public void computeDistances(){
		double xd =0;
		double yd =0;
		for(int i=0; i<x.length; i++){
			for(int j=0; j<x.length; j++){
				if(i != j){
					xd = x[i] - x[j];
					yd = y[i] - y[j];
					g.distance_matrix[i][j] = (int)Math.round(Math.sqrt(xd*xd + yd*yd));
					g.distance_matrix[j][i] = g.distance_matrix[i][j];
				}
				else{
					g.distance_matrix[i][j] = Integer.MAX_VALUE;//highest value?
					g.distance_matrix[j][i] = Integer.MAX_VALUE;//highest value?
				}
			}
		}
	}
	
	public void computeNearestNeighborLists(){
		int[] neighbor = new int[n];
		for(int i = 0; i<n ; i++){//for every city
			for(int j=0; j<n; j++){//fill line for every city with neighbors
				neighbor[j] = g.distance_matrix[i][j];
			}
			Arrays.sort(neighbor);
			replaceDistanceWithCity(i, neighbor);
		}
		if(cl > n)
			cl = n;
	}
	public void replaceDistanceWithCity(int i, int[] neighbor){
		boolean[] citySet = new boolean[n];//flag for every city
		for(int l=0; l<n; l++){
			citySet[l] = false; //mark city as used in neighbor list
		}
		for(int index = 0; index < neighbor.length; index++){//replace all distances in neighbors
			for(int city =0; city < n; city++){//iterate over all cities to find city to which the distance belongs
				if(!citySet[city] && neighbor[index] == g.distance_matrix[i][city]){
					citySet[city] = true;
					neighbor[index] = city;//replace distance to city j with index of city j
				}
			}
		}
	}
	public void computeChoiceInformation(){
		for(int i=0; i<n; i++){
			for(int j=0; j<n; j++){
				g.choice_info[i][j] = g.pheromones[i][j] * 
					Math.pow((1.0/g.distance_matrix[i][j]), beta);
				g.choice_info[j][i] = g.choice_info[i][j];
			}
		}
	}
	public void initializeAnts(){
		this.ant = new TSPAnt[m];
		for(int i=0; i<m; i++){
			ant[i] = new TSPAnt(n);
		}

	}
	//public void initializeParameters(){
		//wurde schon in Konstruktor gemacht
	//}
	public void initializeStatistics(){
		bs_tour_length = Integer.MAX_VALUE;
		bs_tour = new int[n+1];
	}
	public void updatePheromoneTrails(){
		//global pheromone update
		double deltaTau = 1.0 / ((double)this.bs_tour_length);
		int j;
		int l;
		for(int i = 0; i<n; i++){
			j = this.bs_tour[i];
			l = this.bs_tour[i+1];
			g.pheromones[j][l] = (1 - rho)*g.pheromones[j][l] + (rho * deltaTau);
			g.pheromones[l][j] = g.pheromones[j][l];
			g.choice_info[j][l] =  g.pheromones[j][l] * 
	        	Math.pow((1.0/g.distance_matrix[j][l]), beta);
			g.choice_info[l][j] = g.choice_info[l][j];
		}

	}
	public void ACSLocalPheromoneUpdate(int k, int i){
		//local pheromone update after each ant has moved to a new city
		int h = ant[k].tour[i-1]; //previous city which ant k just has left
		int j = ant[k].tour[i];   //new, next city of ant k
		g.pheromones[h][j] = (1 - xi)* g.pheromones[h][j] + (xi * tauZero);
		g.pheromones[j][h] = g.pheromones[h][j]; //symmetric
		g.choice_info[h][j] =  g.pheromones[h][j] * 
			        Math.pow((1.0/g.distance_matrix[h][j]), beta);
		g.choice_info[j][h] = g.choice_info[h][j];
	}
	public void updateStatistics(){
		
		//find best tour and best tour length
		//this.bs_tour_length = ant[0].tour_length;
		//this.bs_tour = ant[0].tour;
		for(int k=0; k<m; k++){      //for every ant k
			if(ant[k].tour_length < this.bs_tour_length){
				this.bs_tour_length = ant[k].tour_length;
				this.bs_tour = ant[k].tour;
			}
		}
		

	}
	public void ACSDecisionRule(int k, int step){
		double q = random.nextDouble();
		if(q < qZero)
			chooseBestNext(k, step);
		else
			NeighborListASDecisionRule(k, step);
			
		
	}
	public void chooseBestNext(int k, int i){
		double v = 0.0;    //argmax
		int c = ant[k].tour[i-1]; //never called with i =0
		int nc =0; //index of the city with highest argmax
		for(int j=0; j<n; j++){
			if(!ant[k].visited[j]){
				if(g.choice_info[c][j] > v){
					nc = j; //index of the city with highest argmax
					v = g.choice_info[c][j];  //update argmax
				}
			}
		}
		ant[k].tour[i] = nc;
		ant[k].visited[nc] = true;
		
	}
	public void NeighborListASDecisionRule(int k, int i){
		int c = ant[k].tour[i-1]; //index of current city of ant k
		double sum_probabilities = 0.0;
		double divisor = 0.0;  //new
		double r;
		double[] selection_probability = new double[cl];
		for(int j=0; j<cl; j++){
			if(ant[k].visited[g.nn_list[c][j]])
				selection_probability[j] = 0.0;
			else{
				selection_probability [j] = g.choice_info[c][g.nn_list[c][j]];
				sum_probabilities = sum_probabilities + selection_probability [j];
				divisor += g.choice_info[c][g.nn_list[c][j]];  //new
			}
		}
		if(sum_probabilities == 0.0)//=>no city on neighbor list
			chooseBestNext(k,i);
		else{
			r = random.nextDouble(); //random.nextInt((int)(sum_probabilities + 1)); //?????Math.random() * sum_probabilities;
			int j = 0;
			double p = selection_probability[j]/divisor;    //selection_probability[j];  : new !!!
			while(p<r){
				j=j+1;
				p = p + selection_probability[j]/divisor;  //p + selection_probability[j];   : new !!!!!!!!!
			}
			ant[k].tour[i] = g.nn_list[c][j];
			ant[k].visited[g.nn_list[c][j]] = true;	
		}
		//random = new Random(random.nextInt());
	}
	public int computeTourLength(int k){
		int length = 0;
		for(int i=0; i<n; i++){
			length = length + g.distance_matrix[ant[k].tour[i]][ant[k].tour[i+1]];
		}
		return length;
	}
	public void innerView(int run){
		//System.out.println();
		//System.out.println();
		System.out.println("run: " + run);
		/*for(int k = 0; k<m; k++){
			System.out.println("Tour f�r ant: " + k);
			for(int i=0; i<ant[k].tour.length; i++)
				System.out.print(" " + ant[k].tour[i]);
			System.out.println();

		}*/

		/*System.out.println("best tour: ");
		for(int i=0; i<this.bs_tour.length; i++)
			System.out.print(" " + this.bs_tour[i]);
		System.out.println();*/
		System.out.println("Best tour length: " +  this.bs_tour_length);	
		if(this.bs_tour_length < 1250)
			System.out.print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		//System.out.println();
		//System.out.println();
	}
	
	public void view(){
		/*for(int k = 0; k<m; k++){
			System.out.println("Tour f�r ant: " + k);
			for(int i=0; i<ant[k].tour.length; i++)
				System.out.print(" " + ant[k].tour[i]);
			System.out.println();

		}*/

		//System.out.println("best tour: ");
		/*for(int i=0; i<this.bs_tour.length; i++)
			System.out.print(" " + this.bs_tour[i]);
		System.out.println();*/
		System.out.println("Best tour length: " +  this.bs_tour_length);


		for(int i = 0; i < g.nodes.size(); i++){
			System.out.print(" " + g.nodes.get(i));
			if(i%10 == 0)
				System.out.println();
		}
		System.out.println();
	}

}
