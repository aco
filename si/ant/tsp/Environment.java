package si.ant.tsp;
import java.util.Vector;
//import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.StringTokenizer;

public class Environment {
	private AntStrategy s;
	private Graph g;
	private int numbOfAnts;
	private int runs;
	
	public Environment(){
		this.s=null;
		this.g=null;
		this.numbOfAnts=0;
		this.runs = 0;
		
	}
	public Environment(Graph g){
		this.s=null;
		this.g=g;
		this.numbOfAnts=0;
		this.runs = 0;
	}
	public Environment(AntStrategy s , int runs){
		this.s = s;
		this.runs = runs;
	}
	public Environment(AntStrategy s, Graph g, int numbOfAnts, int runs){
		this.s = s;
		this.g = g;
		this.numbOfAnts = numbOfAnts;
		this.runs = runs;
		/*ant = new Ant[this.numbOfAnts];
		for(int i=0; i<this.numbOfAnts; i++){
			ant[i]= new Ant(this.s, this.g);
		}*/
	}
	public void simulate(){
		s.runAlgorithm(runs);
		
		
	}
	public static void readTSPFile(String filename, Vector<Integer> x, Vector<Integer> y){
		try{
			BufferedReader tsp = new BufferedReader(new FileReader(filename));
			String s = tsp.readLine();
			//int number;
			boolean beginOfCoordinates = false;
			while(true){
				s = tsp.readLine();
				if(s == null || s.equals("EOF"))
					return;
				if(beginOfCoordinates){
					//s = tsp.readLine();
					StringTokenizer tokens = new StringTokenizer(s);
					tokens.nextToken();  //number of point
					x.add((int)Double.parseDouble(tokens.nextToken())); //x - coordinate
					y.add((int)Double.parseDouble(tokens.nextToken())); //y - coordinate


				}
				if(s.equals("NODE_COORD_SECTION"))
					beginOfCoordinates = true;
			}
			
		}
		catch(FileNotFoundException fn){
			System.out.println("File not found exception : " + fn.getMessage());
		}
		catch(IOException i){
			
		}
		
	}
	
	
	
	public static void main(String[] args){
		
		double tauZero = 8.1E-6;
		double qZero = 0.7;
		int beta = 2;
		double rho = 0.1;
		int cl = 15;
		int runs = 3000000;
		double xi = 0.1;
		
		
		Vector<Integer> xV = new Vector<Integer>();
		Vector<Integer> yV = new Vector<Integer>();
		
		Environment.readTSPFile("C:/Users/Philip/Documents/FH N�rnberg/6. Semester/TSP/rat99.tsp", xV, yV);
		
		
		int[] x = new int[xV.size()];   //{ 1 , 3, 4, 1 };
		int[] y = new int[yV.size()];                       //{ 1 , 1 , 5 , 7 }; 
		for(int i= 0; i<x.length; i++){
			x[i] = xV.get(i);
			y[i] = yV.get(i);
		}
		int cities = x.length;
		int numbOfAnts = 15;
		
		Graph graph = new Graph(cities);
		AntStrategy strategy = new ACSAntAlgorithm(graph, numbOfAnts, cities, x , y , tauZero,beta,  qZero,rho, cl , xi);
		Environment e = new Environment(strategy, runs);
		
		e.simulate();
		strategy.view();
		System.out.println("End of Main");
		
		/*for(int i=0; i<xV.size(); i++){
			System.out.print(" " + xV.get(i));
		}
		System.out.println();
		for(int i=0; i<yV.size(); i++){
			System.out.print(" " + yV.get(i));
		}
		System.out.println();*/
		
		
		
		
		
		
	}
	

}
