package si.ant.tsp;
import java.awt.Point;

public class Node {
	public Point p;
	public String s;
	public Node(String s, int x, int y){
		this.s = s;
		p = new Point(x,y);
	}
	public String toString(){
		return s + " (" + p.x + ", " + p.y + ")";
	}

}
