package si.ant.graph;
import java.util.Vector;

public class ListNode implements Node<ListNode> {
  public ListNode() {
    this.id = "";
  }

  public ListNode(String id) {
    this.id = id;
  }

  public ListNode(ListNode n) {
    this.id = n.getID();
  }

  public Boolean setID(String id) {
    this.id = id;
    if (this.id == id)
      return true;
    else
      return false;
  }

  public String getID() {
    return this.id;
  }

  public Boolean isEqual(ListNode n) {
    if (this.id.equals(n.getID())) {
      return true;
    } else {
      return false;
    }
  }

  @Override public String toString() {
    return "id: " + this.id;
  }

  protected String id;
}
