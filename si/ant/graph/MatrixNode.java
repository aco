package si.ant.graph;
import java.util.Vector;

public class MatrixNode implements Node<MatrixNode> {
  public MatrixNode() {
    this.id = "";
    this.index = 0;
  }

  public MatrixNode(String id) {
    this.id = id;
    this.index = 0;
  }

  public MatrixNode(int index) {
    this.id = "";
    this.index = index;
  }

  public MatrixNode(MatrixNode n) {
    this.id = n.getID();
    this.index = n.getIndex();
  }

  public Boolean setIndex(int index) {
    this.index = index;
    if (this.index == index)
      return true;
    else
      return false;
  }

  public int getIndex() {
    return this.index;
  }

  public Boolean setID(String id) {
    this.id = id;
    if (this.id == id)
      return true;
    else
      return false;
  }

  public String getID() {
    return this.id;
  }

  public Boolean isEqual(MatrixNode n) {
    if (this.index == n.getIndex()) {
      return true;
    } else {
      return false;
    }
  }

  @Override public String toString() {
    return "id: " + this.id + " index: " + ((Integer)this.index).toString();
  }

  protected String id;
  protected int index;
}
