package si.ant.graph;
import java.util.Vector;
import java.util.Iterator;

public class MatrixEdge implements Edge<MatrixEdge, MatrixNode, Double, Double, Double> {
  public MatrixEdge(MatrixNode x, MatrixNode y) {
    nodes = new Vector<MatrixNode>(2);
    nodes.add(x);
    nodes.add(y);
    this.weigth = 0;
    this.distance = 0;
    this.pheromone = 0;
  }

  public MatrixEdge(MatrixNode x, MatrixNode y, double w, double d, double p) {
    nodes = new Vector<MatrixNode>(2);
    nodes.add(x);
    nodes.add(y);
    this.weigth = w;
    this.distance = d;
    this.pheromone = p;
  }

  public Double getWeigth() {
    return this.weigth;
  }

  public Boolean setWeigth(Double w) {
    this.weigth = w;
    if (this.weigth == w)
      return true;
    else
      return false;
  }

  public Double getDistance() {
    return this.distance;
  }

  public Boolean setDistance(Double d) {
    this.distance = d;
    if (this.distance == d)
      return true;
    else
      return false;
  }

  public Double getPheromone() {
    return this.pheromone;
  }

  public Boolean setPheromone(Double p) {
    this.pheromone = p;
    if (this.pheromone == p)
      return true;
    else
      return false;
  }

  public Boolean isEqual(MatrixEdge e) {
    if (this.weigth != e.getWeigth() ||
        this.distance != e.getDistance() ||
        this.pheromone != e.getPheromone()) {
      return false;
    } else if (this.nodes.size() != e.getNodes().size()) {
      return false;
    } else {
      Iterator<MatrixNode> thisit = nodes.iterator();
      Iterator<MatrixNode> thatit = e.getNodes().iterator();
      while (thisit.hasNext() && thatit.hasNext()) {
        if (thisit.next().isEqual(thatit.next())) {
          continue;
        } else {
          return false;
        }
      }
    }

    return true;
  }

  @Override public String toString() {
    StringBuilder thisstr = new StringBuilder();
    Iterator<MatrixNode> nodeit = nodes.iterator();

    while (nodeit.hasNext()) {
      thisstr.append(nodeit.next().getIndex());
      if (nodeit.hasNext()) {
        thisstr.append(" -- ");
        thisstr.append(nodeit.next().getIndex() + "\n");
      }
    }

    thisstr.append("Weigth: " + ((Double)this.weigth).toString() + "\n");
    thisstr.append("Distance: " + ((Double)this.distance).toString() + "\n");
    thisstr.append("Pheromone: " + ((Double)this.pheromone).toString());

    return thisstr.toString();
  }

  public Vector<MatrixNode> getNodes() {
    return this.nodes;
  }

  protected double weigth;
  protected double distance;
  protected double pheromone;

  protected Vector<MatrixNode> nodes;
}
