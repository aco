package si.ant.graph;
import java.util.Vector;

interface Graph<E extends Edge, N extends Node> {
  public Vector<E> getEdges();
  public Vector<N> getNodes();
  public Vector<N> getNeighbours(N n);
  public int getNumNodes();
}

interface Edge<E extends Edge, N extends Node, W, D, P> {
  public W getWeigth();
  public Boolean setWeigth(W w);

  public D getDistance();
  public Boolean setDistance(D d);

  public P getPheromone();
  public Boolean setPheromone(P p);

  public Boolean isEqual(E e);

  public Vector<N> getNodes();
}

interface Node<N extends Node> {
  public Boolean setID(String id);
  public String getID();

  public Boolean isEqual(N n);

}

interface Weigth<T> {
  public T get();
  public Boolean set(T w);
}

interface Distance<T> {
  public T get();
  public Boolean set(T d);
}

interface Pheromone<T> {
  public T get();
  public Boolean set(T p);
}
