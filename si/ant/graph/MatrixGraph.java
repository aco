package si.ant.graph;
import java.util.Vector;

public class MatrixGraph implements Graph<MatrixEdge, MatrixNode> {
  public MatrixGraph(int dim) {
    this.DIMENSION = dim;
  }

  public MatrixGraph(int dim, int[][] w, int[][] d, int[][] p) {
    this.DIMENSION = dim;
    this.WeigthMatrix = w;
    this.DistanceMatrix = d;
    this.PheromoneMatrix = p;
  }

  public Vector<MatrixEdge> getEdges() {
    Vector<MatrixEdge> edges = new Vector<MatrixEdge>();

    for (int x = 0; x < this.DIMENSION; x++) {
      for (int y = 0; y < this.DIMENSION; y++) {
        if ((x != y) && DistanceMatrix[x][y] != MatrixGraph.INFINITY) {
          edges.add(
              new MatrixEdge(
                new MatrixNode(x),
                new MatrixNode(y),
                WeigthMatrix[x][y],
                DistanceMatrix[x][y],
                PheromoneMatrix[x][y]));
        }
      }
    }

    return edges;
  }

  public Vector<MatrixNode> getNodes() {
    Vector<MatrixNode> nodes = new Vector<MatrixNode>();

    for (int x = 0; x < this.DIMENSION; x++) {
      nodes.add(new MatrixNode(x));
    }

    return nodes;
  }

  public int getNumNodes() {
    return DIMENSION;
  }

  public Vector<MatrixNode> getNeighbours(MatrixNode n) {
    int np = n.getIndex();
    Vector<MatrixNode> nodes = new Vector<MatrixNode>();

    for (int x = 0; x < this.DIMENSION; x++) {
      if ((this.DistanceMatrix[x][np] != MatrixGraph.INFINITY) && (np != x))
        nodes.add(new MatrixNode(x));
    }
    for (int y = 0; y < this.DIMENSION; y++) {
      if ((this.DistanceMatrix[np][y] != MatrixGraph.INFINITY) && (np != y))
        nodes.add(new MatrixNode(y));
    }

    return nodes;
  }

  public final int DIMENSION;
  public final static int INFINITY = Integer.MAX_VALUE;

  protected int[][] WeigthMatrix;
  protected int[][] DistanceMatrix;
  protected int[][] PheromoneMatrix;
}
