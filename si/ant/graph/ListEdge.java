package si.ant.graph;
import java.util.Vector;
import java.util.Iterator;

public class ListEdge implements Edge<ListEdge, ListNode, Double, Double, Double> {
  public ListEdge(ListNode x, ListNode y) {
    nodes = new Vector<ListNode>(2);
    nodes.add(x);
    nodes.add(y);
    this.weigth = 0;
    this.distance = 0;
    this.pheromone = 0;
  }

  public ListEdge(ListNode x, ListNode y, double w, double d, double p) {
    nodes = new Vector<ListNode>(2);
    nodes.add(x);
    nodes.add(y);
    this.weigth = w;
    this.distance = d;
    this.pheromone = p;
  }

  public Double getWeigth() {
    return this.weigth;
  }

  public Boolean setWeigth(Double w) {
    this.weigth = w;
    if (this.weigth == w)
      return true;
    else
      return false;
  }

  public Double getDistance() {
    return this.distance;
  }

  public Boolean setDistance(Double d) {
    this.distance = d;
    if (this.distance == d)
      return true;
    else
      return false;
  }

  public Double getPheromone() {
    return this.pheromone;
  }

  public Boolean setPheromone(Double p) {
    this.pheromone = p;
    if (this.pheromone == p)
      return true;
    else
      return false;
  }

  public Boolean isEqual(ListEdge e) {
    if (this.weigth != e.getWeigth() ||
        this.distance != e.getDistance() ||
        this.pheromone != e.getPheromone()) {
      return false;
    } else if (this.nodes.size() != e.getNodes().size()) {
      return false;
    } else {
      Iterator<ListNode> thisit = nodes.iterator();
      Iterator<ListNode> thatit = e.getNodes().iterator();
      while (thisit.hasNext() && thatit.hasNext()) {
        if (thisit.next().isEqual(thatit.next())) {
          continue;
        } else {
          return false;
        }
      }
    }

    return true;
  }

  @Override public String toString() {
    StringBuilder thisstr = new StringBuilder();
    Iterator<ListNode> nodeit = nodes.iterator();

    while (nodeit.hasNext()) {
      thisstr.append(nodeit.next().getID());
      if (nodeit.hasNext()) {
        thisstr.append(" -- ");
        thisstr.append(nodeit.next().getID() + "\n");
      }
    }

    thisstr.append("Weigth: " + ((Double)this.weigth).toString() + "\n");
    thisstr.append("Distance: " + ((Double)this.distance).toString() + "\n");
    thisstr.append("Pheromone: " + ((Double)this.pheromone).toString());

    return thisstr.toString();
  }

  public Vector<ListNode> getNodes() {
    return this.nodes;
  }

  protected double weigth;
  protected double distance;
  protected double pheromone;

  protected Vector<ListNode> nodes;
}
