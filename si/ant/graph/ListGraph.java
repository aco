package si.ant.graph;
import java.util.Vector;
import java.util.Iterator;
import java.util.ListIterator;

public class ListGraph implements Graph<ListEdge, ListNode> {
  public ListGraph() {
    edges = new Vector<ListEdge>();
  }

  public ListGraph(Vector<ListEdge> edges) {
    this.edges = new Vector<ListEdge>(edges);
  }

  public Vector<ListEdge> getEdges() {
    return edges;
  }

  public Vector<ListNode> getNodes() {
    Vector<ListNode> nodes = new Vector<ListNode>(edges.size()*2);
    Iterator<ListEdge> edgeit = edges.iterator();

    while (edgeit.hasNext()) {
      Iterator<ListNode> nodeit = edgeit.next().getNodes().iterator();
      while (nodeit.hasNext()) {
        ListNode tmp = nodeit.next();
        if (!nodes.contains(tmp))
          nodes.add(tmp);
      }
    }

    return nodes;
  }

  public int getNumNodes() {
    return this.getNodes().size();
  }

  public Vector<ListNode> getNeighbours(ListNode n) {
    Vector<ListNode> neighbours = new Vector<ListNode>();
    Iterator<ListEdge> edgeit = edges.iterator();

    while (edgeit.hasNext()) {
      /* get set of nodes for this edge */
      Vector<ListNode> nodes = edgeit.next().getNodes();
      /* if Node n is in nodes, then remove will return true */
      if (nodes.remove(n)) {
        /* iterator over remaining nodes */
        Iterator <ListNode> nodeit = nodes.iterator();
        while (nodeit.hasNext()) {
          ListNode tmp = nodeit.next();
          /* if node is not already in neighbour's list.. */
          if (!neighbours.contains(tmp))
            /* add it */
            neighbours.add(tmp);
        }
      }
    }

    return neighbours;
  }

  Vector<ListEdge> edges;
}
